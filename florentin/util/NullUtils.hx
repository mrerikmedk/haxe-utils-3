package florentin.util;

class NullUtils {
    static public function nullOrelse<T>(left : Null<T>, right : Null<T>) : Null<T> {
        return if (left != null) left else right;
    }
    static public function nullFallback<T>(left : Null<T>, right : T) : T {
        return if (left != null) left else right;
    }
    static public function nullMap<S, T>(v : Null<S>, f : S->T) : Null<T> {
        return if (v != null) f(v) else null;
    }

}