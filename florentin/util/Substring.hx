package florentin.util;

using florentin.util.Sequence;

typedef Substring = {
    s : String,
    i : Int,
    n : Int
}


class SubstringUtils {

    static public function show(s : Substring) : String {
        return s.s.substr(s.i, s.n);
    }

    static public function substring(s : Substring, i : Int, n : Int) : Substring {
        return if (0 <= i && 0 <= n && i + n <= s.n) {
            s: s.s, i: s.i + i, n: n
        } else throw "Indices out of range.";
    }

    static public function eq(s1 : Substring, s2 : Substring) : Bool {
        return s1.i == s2.i
            && s1.n == s2.n
            && s1.s == s2.s;
    }

}