package florentin.util;

using Lambda;
using Map;
using florentin.util.IteratorUtils;
using florentin.util.MapUtils;

abstract Set<T>(Map<T, T>) {
    inline public function new(m : Map<T, T>) {
        this = m;
    }

    public function isEmpty() : Bool {
        return this.keys().emptyIterator();
    }

    public function contains(v : T) : Bool {
        return this.exists(v);
    }

    public function insert(v : T) : Set<T> {
        var s2 = new Set<T>(this.copy());
        s2.insertMutate(v);
        return s2;
    }
    public function inserts(vs : Iterable<T>) : Set<T> {
        var s2 = copy();
        for (v in vs) {
            s2.insertMutate(v);
        }
        return s2;
    }

    public function remove(v : T) : Set<T> {
        var s2 = copy();
        s2.removeMutate(v);
        return s2;
    }
    public function removes(vs : Iterable<T>) : Set<T> {
        var s2 = new Set<T>(this.copy());
        for (v in vs) {
            s2.removeMutate(v);
        }
        return s2;
    }

    public function union(s2 : Set<T>) : Set<T> {
        return s2.inserts(this.keys2());
    }
    public function eq(s2 : Set<T>) : Bool {
        for (v in this) {
            if (!s2.contains(v)) {
                return false;
            }
        }
        for (v in s2) {
            if (!contains(v)) {
                return false;
            }
        }
        return true;
    }

    public function iterator() : Iterator<T> {
        return this.keys();
    }
    public function values() : Iterable<T> {
        return this.keys2();
    }
    public function array() : Array<T> {
        return this.keys2().array();
    }

    public function insertMutate(v : T) : Void {
        this.set(v, v);
    }
    public function removeMutate(v : T) : Bool {
        return this.remove(v);
    }
    public function copy() : Set<T> {
        return new Set<T>(this.copy());
    }

    public function toString() : String {
        return SetUtils.show(abstract, v->'$v');
    }
}

class SetUtils {
    // Make empty set.
    static public function emptyString() : Set<String> {
        return new Set<String>(new Map());
    }
    // Make singleton set.
    static public function singletonString(v : String) : Set<String> {
        var s = emptyString();
        s.insertMutate(v);
        return s;
    }
    // Make a set from a sequence.
    static public function setOfStrings(vs : Iterable<String>) : Set<String> {
        var s = emptyString();
        for (v in vs) {
            s.insertMutate(v);
        }
        return s;
    }

    // Union of a number of sets.
    static public function unionsString(ss : Iterable<Set<String>>) : Set<String> {
        return ss.fold((s, su : Set<String>)->su.union(s), emptyString());
    }

    static public function show<T>(
        set : Set<T>,
        showElement : T->String,
        delimiter : String = ", ",
        braces : String->String = null
    ) : String {
        if (braces == null) {
            braces = s->"{" + s + "}";
        }
        return braces(set.values().showElements(showElement, delimiter));
    }
}