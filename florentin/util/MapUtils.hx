package florentin.util;

using haxe.Constraints;
using florentin.util.IteratorUtils;
import florentin.util.KeyValue;

class MapUtils {

    // Converts a map into a total function defaulting to `fallback` when 
    // there is no association in the map.
    static public function totalFun<TK, TV>(m : Map<TK, TV>, fallback : TV) : TK->TV {
        return function(key) return
            if (m.exists(key)) m.get(key) else fallback;
    }
    static public function partialFun<TK, TV>(m : Map<TK, TV>) : TK->TV {
        return function(key) return
            if (m.exists(key)) m.get(key)
            else throw 'Undefined for $key.';
    }

    // Shows a map as a string. E.g. 
    //   showMapElements(["x"=>1, "y"=>2], Std.string, Std.string, ": ", ", ")
    //   ==> "x: 1, y:2"
    static public function showMapElements<TK, TV>(m : Map<TK, TV>, 
        showKey : TK->String, showValue : TV->String, 
        mapsto : String, delimiter : String
    ) : String {
        return m.keys().showElementsIterator(
            function(key) return '${showKey(key)}$mapsto${showValue(m.get(key))}',
            delimiter
        );
    }

    // True iff `m1` and `m2` are equal.
    // The two maps must have the same keys and the values associated with the 
    // keys must be equal by `eq`.
    static public function eqBy<TK, TV1, TV2>(
        m1 : Map<TK, TV1>,
        m2 : Map<TK, TV2>,
        eq : TV1->TV2->Bool
    ) : Bool {
        for (pair in pairs(m1)) {
            if (!m2.exists(pair.key)
             || !eq(pair.value, m2.get(pair.key))) {
                return false;
            }
        }
        for (key in m2.keys()) {
            if (!m1.exists(key)) {
                return false;
            }
        }
        return true;
    }

    // True iff `as` is a permutation of `bs` up to the equality `eq`.
    // Both sequences must be capable of being uniquely indexed by `getKey`.
    static public function permutationString<T>(
        as : Iterable<T>,
        bs : Iterable<T>,
        getKey : T->String,
        eq : T->T->Bool
    ) : Bool {
        return permutationGeneralString(as, bs, getKey, getKey, eq);
    }
    static public function permutationStringC<T>(
        getKey : T->String,
        eq : T->T->Bool
    ) : Iterable<T>->Iterable<T>->Bool {
        return function (as, bs) return permutationString(as, bs, getKey, eq);
    }
    static public function permutationGeneralString<T1, T2>(
        as : Iterable<T1>,
        bs : Iterable<T2>,
        getKeya : T1->String,
        getKeyb : T2->String,
        eq : T1->T2->Bool
    ) : Bool {
        return eqBy(
            indexString(as, getKeya),
            indexString(bs, getKeyb),
            eq
        );
    }

    // Sets a key, value association in map if it is not already set (changes 
    // map).
    // Mutates `map`.
    static public function setUnder<TK, TV>(map : Map<TK, TV>, key : TK, value : TV) : Void {
        if (!map.exists(key)) {
            map.set(key, value);
        }
    }

    static public function putString<TV>(
        map : Map<String, TV>,
        key : String,
        value : TV
    ) : Map<String, TV> {
        var copy = copySpineString(map);
        copy.set(key, value);
        return copy;
    }
    static public function putInt<TV>(
        map : Map<Int, TV>,
        key : Int,
        value : TV
    ) : Map<Int, TV> {
        var copy = copySpineInt(map);
        copy.set(key, value);
        return copy;
    }

    static public function eraseString<TV>(
        map : Map<String, TV>,
        key : String
    ) : Map<String, TV> {
        var copy = copySpineString(map);
        copy.remove(key);
        return copy;
    }

    static public function transformString<TV>(
        map : Map<String, TV>,
        key : String,
        f : TV->TV
    ) : Map<String, TV> {
        var v = map.get(key);
        return if (v != null) putString(map, key, f(v))
        else map;
    }

    // Updates the `map` at `key` by applying the `update` function to the 
    // value. If the `key` is not associated, associates `update(z)` to it.
    // Returns the previous value at `key`.
    // Mutates `map`.
    static public function updateTotal<TK, TV>(
        map : Map<TK, TV>,
        key : TK, 
        z : TV, 
        update : TV->TV
    ) : Void {
        var v0 = if (map.exists(key)) map.get(key) else z;
        map.set(key, update(v0));
    }

    static public function getSafe<TK, TV>(map : Map<TK, TV>, key : TK, fallback : TV) : TV {
        return if (map.exists(key)) map.get(key) else fallback;
    }

    // Applies a function to all values in map.
    static public function mapValuesString<TV1, TV2>(map : Map<String, TV1>, f : TV1->TV2) : Map<String, TV2> {
        var result = new Map<String, TV2>();
        for (pair in pairs(map)) {
            result.set(pair.key, f(pair.value));
        }
        return result;
    }

    // Applies a function to all values in map. The function takes key, value.
    static public function mapValuesString1<TV1, TV2>(
        map : Map<String, TV1>, 
        f : String->TV1->TV2
    ) : Map<String, TV2> {
        var result = new Map<String, TV2>();
        for (pair in pairs(map)) {
            result.set(pair.key, f(pair.key, pair.value));
        }
        return result;
    }

    // Filters out all key, value associations in map that do not satisfy the 
    // value predicate.
    static public function filterValuesString<TV>(
        map : Map<String, TV>, 
        p : TV->Bool
    ) : Map<String, TV> {
        var result = new Map<String, TV>();
        for (pair in pairs(map)) {
            if (p(pair.value)) {
                result.set(pair.key, pair.value);
            }
        }
        return result;
    }

    // Pointwise union of two maps where the associations in over take 
    // precedence over associations in under.
    static public function unionOverString<TV>(
        under : Map<String, TV>, 
        over : Map<String, TV>
    ) : Map<String, TV> {
        var result = new Map<String, TV>();
        for (pair in pairs(under)) {
            result.set(pair.key, pair.value);
        }
        for (pair in pairs(over)) {
            result.set(pair.key, pair.value);
        }
        return result;
    }
    static public function unionBy<TK, TV>(
        m1 : Map<TK, TV>,
        m2 : Map<TK, TV>,
        combine : TV->TV->TV
    ) : Map<TK, TV> {
        var m3 = m1.copy();
        for (pair in pairs(m2)) {
            var value1 = m1.get(pair.key);
            if (value1 != null) {
                m3.set(pair.key, combine(value1, pair.value));
            } else {
                m3.set(pair.key, pair.value);
            }
        }
        return m3;
    }

    static public function copySpineString<TV>(m : Map<String, TV>) : Map<String, TV> {
        var m2 = new Map<String, TV>();
        for (pair in pairs(m)) {
            m2.set(pair.key, pair.value);
        }
        return m2;
    }
    static public function copySpineInt<TV>(m : Map<Int, TV>) : Map<Int, TV> {
        var m2 = new Map<Int, TV>();
        for (pair in pairs(m)) {
            m2.set(pair.key, pair.value);
        }
        return m2;
    }

    // Converts a sequence of key, value pairs into a map. Chooses one value 
    // iff multiple instances of the same key appears in the sequence.
    static public function fromPairsString<TV>(iter : Iterable<KeyValue<String, TV>>) : Map<String, TV> {
        return fromPairsStringIterator(iter.iterator());
    }
    static public function fromPairsStringIterator<TV>(iter : Iterator<KeyValue<String, TV>>) : Map<String, TV> {
        var result = new Map<String, TV>();
        for (pair in iter) {
            result.set(pair.key, pair.value);
        }
        return result;
    }

    static public function zipToMapString<TV>(
        keys : Iterable<String>, values : Iterable<TV>
    ) : Map<String, TV> {
        var result = new Map<String, TV>();
        var ks = keys.iterator();
        var vs = values.iterator();
        while (ks.hasNext() && vs.hasNext()) {
            result.set(ks.next(), vs.next());
        }
        return result;
    }

    // Indexes the elements of iter into a map by a key given by the getKey 
    // function. Only one value will picked for multiple occurrences of the 
    // same key.
    //TODO: unit test
    static public function indexString<T>(
        iter : Iterable<T>, getKey : T->String
    ) : Map<String, T> {
        return fromElementsString(iter, getKey, function(v) return v);
    }
    static public function fromElementsString<T, TV>(
        iter : Iterable<T>,
        getKey : T->String, getValue : T->TV
    ) : Map<String, TV> {
        return fromCombinedElementsString(iter, getKey, getValue, function(v, _) return v);
    }

    static public function fromCombinedElementsString<T, TV>(
        iter : Iterable<T>,
        getKey : T->String, getValue : T->TV, combineValue : TV->T->TV
    ) : Map<String, TV> {
        var result = new Map<String, TV>();
        for (e in iter) {
            var key = getKey(e);
            if (result.exists(key)) {
                result.set(key, combineValue(result.get(key), e));
            } else {
                result.set(key, getValue(e));
            }
        }
        return result;
    }

    // Rerturns all keys as an iterable (`keys` in `Array.hx` returns an 
    // `Iterator`). Useful when chaining iterables.
    static public function keys2<TK, TV>(m : Map<TK, TV>) : Iterable<TK> {
        return {
            iterator: m.keys
        }
    }
    static public function values2<TK, TV>(m : Map<TK, TV>) : Iterable<TV> {
        return {
            iterator: m.iterator
        }
    }

    // Returns all key, value pairs of m as an iterable.
    static public function pairs<TK, TV>(m : Map<TK, TV>) : Iterable<KeyValue<TK, TV>> {
        return {
            iterator: function() return pairsIterator(m)
        };
    }
    static public function pairsIterator<TK, TV>(m : Map<TK, TV>) : Iterator<KeyValue<TK, TV>> {
        var keys = m.keys();
        return {
            hasNext: function() return keys.hasNext(),
            next: function() {
                var key = keys.next();
                return {key: key, value: m.get(key)};
            }
        };
    }

    // Merges the entries of two maps into 
    @:generic
    static public function mergeBy<TK, TV>(
        left : Map<TK, TV>, right : Map<TK, TV>,
        merge : TV->TV->TV
    ) : Map<TK, TV> {
        return mergeByInto(left, right, merge, new Map());
    }
    static public function mergeByInto<TK, TV>(
        left : Map<TK, TV>, right : Map<TK, TV>,
        merge : TV->TV->TV,
        dest : Map<TK, TV>
    ) : Map<TK, TV> {
        return mergeMapsGeneralizedInto(left, right, 
            function(v) return v,
            function(v) return v,
            merge,
            dest
        );
    }

    @:generic
    static public function mergeMapsGeneralized<TK, TV1, TV2, TV>(
        left : Map<TK, TV1>, right : Map<TK, TV2>,
        injectLeft : TV1->TV, injectRight : TV2->TV, merge : TV1->TV2->TV
    ) : Map<TK, TV> {
        return mergeMapsGeneralizedInto(left, right, injectLeft, injectRight, merge, new Map());
    }
    static public function mergeMapsGeneralizedInto<TK, TV1, TV2, TV>(
        left : Map<TK, TV1>, right : Map<TK, TV2>,
        injectLeft : TV1->TV, injectRight : TV2->TV, merge : TV1->TV2->TV,
        result : Map<TK, TV>
    ) : Map<TK, TV> {
        var trisection = trisectKeys(left, right);
        for (key in trisection.inLeft) {
            result.set(key, injectLeft(left.get(key)));
        }
        for (key in trisection.inRight) {
            result.set(key, injectRight(right.get(key)));
        }
        for (key in trisection.inBoth) {
            result.set(key, merge(left.get(key), right.get(key)));
        }
        return result;
    }

    // Divides the keys of the two maps into three groups: 
    //   `inBoth`: Keys occurring in both `left` and `right`.
    //   `inLeft`: Keys only occurring in `left`.
    //   `inRight`: Keys only occurring in `right`.
    static public function trisectKeys<TK, TV1, TV2>(
        left : Map<TK, TV1>, right : Map<TK, TV2>
    ) : {inLeft : Iterable<TK>, inBoth : Iterable<TK>, inRight : Iterable<TK>} {
        return {
            inLeft: keys2(left).filterLazy(function(k) return !right.exists(k)),
            inBoth: keys2(left).filterLazy(function(k) return right.exists(k)),
            inRight: keys2(right).filterLazy(function(k) return !left.exists(k))
        };
    }

}
