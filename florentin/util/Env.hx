package florentin.util;

using florentin.util.MapUtils;

enum Env<TK, TV> {
    Root;
    Set(key : TK, value : TV, next : Env<TK, TV>);
    Map(map : Map<TK, TV>, next : Env<TK, TV>);
}

class EnvUtils {
    // Creates the empty environment.
    static public function empty<TK, TV>() : Env<TK, TV> {
        return Root;
    }

    // Converts a map to an environment.
    // The environment references the map.
    static public function mapToEnv<TK, TV>(m : Map<TK, TV>) : Env<TK, TV> {
        return Map(m, Root);
    }

    // Sets a value in the environment.
    static public function set<TK, TV>(
        env : Env<TK, TV>, 
        k : TK, v : TV
    ) : Env<TK, TV> {
        return Set(k, v, env);
    }
    
    static public function setsString<TV>(
        env : Env<String, TV>,
        xs : Iterable<String>,
        vs : Iterable<TV>
    ) : Env<String, TV> {
        return setMap(env, xs.zipToMapString(vs));
    }

    // Sets a map of associations in the environment.
    // The environment references the map.
    static public function setMap<TK, TV>(env : Env<TK, TV>, map : Map<TK, TV>) : Env<TK, TV> {
        return Map(map, env);
    }

    @:obsolete //use mapToEnv
    static public function fromMap<TK, TV>(map : Map<TK, TV>) : Env<TK, TV> {
        return setMap(empty(), map);
    }

    // True iff the environment associates a value to a key.
    static public function has<TK, TV>(env : Env<TK, TV>, key : TK) : Bool {
        return splitGet(env, key, function(_) return true, function() return false);
    }

    // Gets the associated value of `key`. 
    // Null if there are no associations.
    static public function tryGet<TK, TV>(env : Env<TK, TV>, key : TK) : Null<TV> {
        return splitGet(env, key, function(v) return v, function() return null);
    }

    // Combines two environments where associations in `over` takes precedence 
    // over associations in `under`.
    static public function concat<TK, TV>(under : Env<TK, TV>, over : Env<TK, TV>) : Env<TK, TV> {
        return switch (over) {
            case Root: under;
            case Set(k, v, next): Set(k, v, concat(under, next));
            case Map(map, next): Map(map, concat(under, next));
        }
    }

    // Generalized get function.
    // Calls either of the two continuations on success or failure.
    static public function splitGet<TK, TV, T>(
        env : Env<TK, TV>, 
        key : TK,
        onFound : TV->T,
        onMissing : Void->T
    ) : T {
        var work = env;
        while (true) switch (work) {
            case Root: return onMissing();
            case Set(k, v, next): 
                if (k == key) return onFound(v)
                else work = next;
            case Map(map, next):
                if (map.exists(key)) {
                    return onFound(map.get(key));
                } else {
                    work = next;
                }
        }
    }

    // Maps all values in the environment.
    static public function mapValuesString<TV1, TV2>(
        env : Env<String, TV1>,
        f : TV1->TV2
    ) : Env<String, TV2> {
        return mapToEnv(toMapString(env).mapValuesString(f));
    }

    // Shows the environment.
    static public function showEntriesString<TV>(
        env : Env<String, TV>,
        showKey : String->String,
        showValue : TV->String,
        delimiter : String = ", ",
        mapsTo : String = ": "
    ) : String {
        var flattened = flattenString(env);
        return switch (flattened) {
            case Map(map, _):
                map.showMapElements(showKey, showValue, mapsTo, delimiter);
            default: throw "surprise";
        }
    }

    // Optimizes an environment by cleaning covered associations and making 
    // lookups more efficient.
    static public function flattenString<TV>(env : Env<String, TV>) : Env<String, TV> {
        return fromMap(toMapString(env));
    }

    // Extracts all associations in the environment as a map.
    static public function toMapString<TV>(env : Env<String, TV>) : Map<String, TV> {
        var work = env;
        var map = new Map<String, TV>();
        while (!isRoot(work)) switch (work) {
            case Set(k, v, next):
                if (!map.exists(k)) map.set(k, v);
                work = next;
            case Map(map, next):
                for (pair in map.pairs()) {
                    if (!map.exists(pair.key)) map.set(pair.key, pair.value);
                }
                work = next;
            default: throw "surprise";
        }
        return map;
    }

    // True iff the environment is empty.
    static public function isRoot<TK, TV>(env : Env<TK, TV>) : Bool {
        return switch (env) {
            case Root: true;
            default: false;
        }
    }

}
