package florentin.util;

import haxe.Log;
using haxe.PosInfos;
using florentin.util.StringUtils;

class TraceIndent {
    static public function install() {
        oldTrace = Log.trace;
        Log.trace = trace;
    }

    static public function trace(msg : String, ?p : PosInfos) : Void {
        var ind = "  ".repeat(indent);
        oldTrace(ind + msg, p);
    }
    static public function traceWrap<T>(f : Void->T, msg : String) : T {
        traceIn('$msg {');
        var result = f();
        traceOut('} $msg');
        return result;
    }
    static public function traceIn(msg : String, ?p : PosInfos) : Void {
        Log.trace(msg, p);
        ++indent;
    }
    static public function traceOut(msg : String, ?p : PosInfos) : Void {
        --indent;
        Log.trace(msg, p);
    }

    static private var indent : Int = 0;
    static private var oldTrace : String->PosInfos->Void;
}