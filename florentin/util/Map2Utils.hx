package florentin.util;

using florentin.util.IteratorUtils;

typedef Map2<TKey1, TKey2, TValue> = Map<TKey1, Map<TKey2, TValue>>;

// Maps from two keys to values (via curried `Map`s).
class Map2Utils {
    // Sets (or overwrites) the value at (`key1`, `key2`) in the map.
    // Changes `m`.
    @:generic
    static public function set2<TKey1, TKey2, TValue>(
        m : Map2<TKey1, TKey2, TValue>, 
        key1 : TKey1, key2 : TKey2, value : TValue
    ) : Void {
        var m2 = m.get(key1);
        if (m2 == null) {
            m2 = new Map<TKey2, TValue>();
            m.set(key1, m2);
        }
        m2.set(key2, value);
    }

    // True iff there is a value set at (`key1`, `key2`) in the map.
    static public function has2<TKey1, TKey2, TValue>(
        m : Map2<TKey1, TKey2, TValue>,
        key1 : TKey1, key2 : TKey2
    ) : Bool {
        var m2 = m.get(key1);
        return m2 != null && m2.exists(key2);
    }

    // Gets the value at (`key1`, `key2`) if there is one. Otherwise null.
    static public function get2<TKey1, TKey2, TValue>(
        m : Map2<TKey1, TKey2, TValue>,
        key1 : TKey1, key2 : TKey2
    ) : Null<TValue> {
        return getSplit2(m, key1, key2, function(x) return x, function() return null);
    }
    
    // Looks up the value at (`key1`, `key2`). Returns `onSome(v)` if there is 
    // a value `v`. Otherwise `onNone()`.
    static public function getSplit2<TKey1, TKey2, TValue, T>(
        m : Map2<TKey1, TKey2, TValue>,
        key1 : TKey1, key2 : TKey2,
        onSome : TValue->T,
        onNone : Void->T
    ) : T {
        var m2 = m.get(key1);
        return if (m2 != null) {
            var v = m2.get(key2);
            onSome(v);
        } else onNone();
    }

    // Removes the element at (`key1`, `key2`) from the map if there is some. 
    // Otherwise nop.
    // Mutates the map.
    static public function remove2<TKey1, TKey2, TValue>(
        m : Map2<TKey1, TKey2, TValue>,
        key1 : TKey1, key2 : TKey2
    ) : Void {
        var m2 = m.get(key1);
        if (m2 != null) {
            m2.remove(key2);
            if (m2.keys().emptyIterator()) {
                m.remove(key1);
            }
        }
    }
}
