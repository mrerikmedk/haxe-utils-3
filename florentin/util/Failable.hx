package florentin.util;

typedef Failable<T> = FailableBy<T, String>;
enum FailableBy<T, TE> {
    Ok(v : T);
    Failed(e : TE);
}

class FailableUtils {

    static public function ok<T, TE>(
        v : T
    ) : FailableBy<T, TE> {
        return Ok(v);
    }

    static public function failed<T, TE>(
        e : TE
    ) : FailableBy<T, TE> {
        return Failed(e);
    }

    static public function mbind<T1, T2, TE>(
        o1 : FailableBy<T1, TE>, 
        o2 : T1->FailableBy<T2, TE>
    ) : FailableBy<T2, TE> {
        return switch (o1) {
            case Ok(v): o2(v);
            case Failed(e): Failed(e);
        }
    }

    static public function mmap<T1, T2, TE>(
        o : FailableBy<T1, TE>,
        f : T1->T2
    ) : FailableBy<T2, TE> {
        return switch (o) {
            case Ok(v): Ok(f(v));
            case Failed(e): Failed(e);
        }
    }

    static public function mbindError<T, TE1, TE2>(
        o1 : FailableBy<T, TE1>,
        o2 : TE1->FailableBy<T, TE2>
    ) : FailableBy<T, TE2> {
        return switch (o1) {
            case Ok(v): Ok(v);
            case Failed(e): o2(e);
        }
    }

    static public function mmapError<T, TE1, TE2>(
        o : FailableBy<T, TE1>,
        f : TE1->TE2
    ) : FailableBy<T, TE2> {
        return switch (o) {
            case Ok(v): Ok(v);
            case Failed(e): Failed(f(e));
        }
    }

    static public function handle<T, TE>(
        o : FailableBy<T, TE>,
        f : TE->T
    ) : FailableBy<T, TE> {
        return switch (o) {
            case Ok(v): Ok(v);
            case Failed(e): Ok(f(e));
        }
    }

}
