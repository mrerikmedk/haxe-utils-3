package florentin.util;

using Lambda;
using String;
using florentin.util.HexString;
using florentin.util.StringUtils;

class HxEscape {
    static public function hxEscape(s : String) : String {
        return '"${hxEscapeChars(s)}"';
    }

    static public function hxEscapeChars(s : String) : String {
        return s.codes().fold(
            function(c, s) : String return s + hxEscapeChar(c),
            ""
        );
    }

    static public function hxEscapeChar(c : Int) : String {
        return if (0x20 <= c && c <= 0x7E 
                && c != "\\".code && c != "\"".code && c != "'".code) c.fromCharCode();
        else switch (c) {
            case "\\".code: "\\\\";
            case "\n".code: "\\n";
            case "\r".code: "\\r";
            case "\t".code: "\\t";
            case "\"".code: "\\\"";
            case "'".code: "\\'";
            default: 
                if (c <= 0xFF) "\\x" + c.toBaseField(16, 2)
                else if (c <= 0xFFFF) "\\u" + c.toBaseField(16, 4)
                else throw "Can't escape char code";
        }
    }
}
