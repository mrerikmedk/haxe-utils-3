package florentin.util;

using florentin.util.MapUtils;

typedef Lens<TC, TE> = LensGeneralized<TC, TE, TC, TE>;
typedef LensGeneralized<TC1, TE1, TC2, TE2> = {
    get : TC1->TE1,
    set : TC1->TE2->TC2
}

class LensUtils {
    // Composes two lenses.
    static public function compose<TC1, TI1, TE1, TC2, TI2, TE2>(
        l1 : LensGeneralized<TC1, TI1, TC2, TI2>,
        l2 : LensGeneralized<TI1, TE1, TI2, TE2>
    ) : LensGeneralized<TC1, TE1, TC2, TE2> {
        return {
            get: function(c) return l2.get(l1.get(c)),
            set: function(c, e) return l1.set(c, l2.set(l1.get(c), e))
        };
    }

    // Transforms a field.
    static public function update<TC1, TE1, TC2, TE2>(
        c : TC1,
        l : LensGeneralized<TC1, TE1, TC2, TE2>,
        f : TE1->TE2
    ) : TC2 {
        return l.set(c, f(l.get(c)));
    }
    static public function updateC<TC1, TE1, TC2, TE2>(
        l : LensGeneralized<TC1, TE1, TC2, TE2>,
        f : TE1->TE2
    ) : TC1->TC2 {
        return function(c) return update(c, l, f);
    }

    static public function mapAtStringLens<TV>(
        key : String
    ) : Lens<Map<String, TV>, Null<TV>> {
        return {
            get: function(map) return map.get(key),
            set: function(map, v) 
                return if (v != null) map.putString(key, v)
                else map.eraseString(key)
        }
    }
}