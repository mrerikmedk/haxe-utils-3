package florentin.util;

using StringTools;

class CharUtils {
    // Converts a character digit to its value:
    //   fromDigit("7".code) ==> 7
    // Returns something weird for non-digits.
    // todo: unit test
    public static function fromDigit(c : Int) : Int {
        return c - "0".code;
    }

    // Convers a character base digit to its value.
    //   fromBaseDigit("7".code) ==> 7
    //   fromBaseDigit("F".code) ==> 15
    //   fromBaseDigit("G".code) ==> 16
    // Returns something weird for non-digits.
    // todo: unit test
    public static function fromBaseDigit(c : Int) : Int {
        return if ("0".code <= c && c <= "9".code) c - "0".code
        else if ("a".code <= c && c <= "z".code) c - "a".code + 10
        else if ("A".code <= c && c <= "Z".code) c - "A".code + 10
        else -1;
    }

    // Converts a digit value to a base digit. 
    //   toBaseDigit(7) ==> "7".code
    //   toBaseDigit(15) ==> "F".code
    // Explodes if the value is out of range.
    // todo: determine range
    // todo: unit test
    public static function toBaseDigit(i : Int) : Int {
        return "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".fastCodeAt(i);
    }

    // Iterates over all characters in the string.
    public static function codes(s : String) : Iterable<Int> {
        return {
            iterator: function() return {
                var i = 0;
                {
                    hasNext: function() return i < s.length,
                    next: function() return {
                        var c = s.fastCodeAt(i);
                        ++i;
                        c;
                    }
                }
            }
        }
    }
   
}