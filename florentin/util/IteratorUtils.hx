package florentin.util;

using Lambda;

class IteratorUtils {

    // True iff there are no elements in iter (lazy).
    static public function empty<T>(iter : Iterable<T>) : Bool {
        return !iter.iterator().hasNext();
    }
    static public function emptyIterator<T>(iter : Iterator<T>) : Bool {
        return !iter.hasNext();
    }
    
    // True iff iter has at least n elements (lazy).
    static public function atLeast<T>(iter : Iterable<T>, n : Int) : Bool {
        return atLeastIterator(iter.iterator(), n);
    }
    static public function atLeastIterator<T>(iter : Iterator<T>, n : Int) : Bool {
        for (i in 0...n) {
            if (iter.hasNext()) {
                // todo: last element does not need to next?
                iter.next();
            } else {
                return false;
            }
        }
        return true;
    }

    // Returns an iterator that yields the first n elements of iter. If there 
    // are fewer than n elements available in iter, yields iter (lazy).
    static public function take<T>(iter : Iterable<T>, n : Int) : Iterable<T> {
        return {
            iterator: function() return takeIterator(iter.iterator(), n)
        };
    }
    static public function takeIterator<T>(iter : Iterator<T>, n : Int) : Iterator<T> {
        return {
            hasNext: function() return n > 0 && iter.hasNext(),
            next: function() {
                --n;
                return iter.next();
            }
        };
    }

    static public function head<T>(xs : Iterable<T>) : T {
        var iter = xs.iterator();
        return if (iter.hasNext()) iter.next()
        else throw("head: Empty sequence.");
    }

    // Returns an iterator that yields the elements of iter but the first n
    // elements. (lazy)
    //todo: unit test.
    static public function drop<T>(iter : Iterable<T>, n : Int) : Iterable<T> {
        return {
            iterator: function() return dropIterator(iter.iterator(), n)
        };
    }
    static public function dropIterator<T>(iter : Iterator<T>, n : Int) : Iterator<T> {
        for (i in 0...n) {
            iter.next();
        }
        return iter;
    }

    static public function filterLazy<T>(xs : Iterable<T>, p : T->Bool) : Iterable<T> {
        return {
            iterator: function() return {
                var it = xs.iterator();
                if (it.hasNext()) {
                    var next = it.next();
                    var hasNext = p(next);
                    while (!hasNext && it.hasNext()) {
                        next = it.next();
                        hasNext = p(next);
                    }
                    if (hasNext) {
                        hasNext: function() return hasNext,
                        next: function() return {
                            var theNext = next;
                            if (it.hasNext()) {
                                next = it.next();
                                hasNext = p(next);
                                while (!hasNext && it.hasNext()) {
                                    next = it.next();
                                    hasNext = p(next);
                                }
                            } else {
                                hasNext = false;
                            }
                            theNext;
                        }
                    } else {
                        hasNext: function() return false,
                        next: function() throw "no next"
                    }
                } else {
                    hasNext: function() return false,
                    next: function() throw "no next"
                }
            }
        }
    }

    // Maps `f` on `xs` filtering out all nulls.
    static public function filterMap<T1, T2>(
        xs : Iterable<T1>, 
        f : T1->Null<T2>
    ) : Iterable<T2> {
        // TODO: Do in one pass.
        // TODO: Something screwy is going on without array. It seems to 
        // unwind too far at weird times.
        return filterLazy(
            mapLazy(xs, f),
            y->y != null
        );
    }

    // True iff all members of `xs` satisfy `p`.
    //OBSOLETE: Use Lambda.foreach
    static public function allThat<T>(xs : Iterable<T>, p : T->Bool) : Bool {
        for (x in xs) {
            if (!p(x)) {
                return false;
            }
        }
        return true;
    }

    // True iff at least one member of `xs` satisfy `p`.
    //OBSOLETE: Use Lambda.exists
    static public function oneThat<T>(xs : Iterable<T>, p : T->Bool) : Bool {
        for (x in xs) {
            if (p(x)) {
                return true;
            }
        }
        return false;
    }

    static public function contains<T>(xs : Iterable<T>, x : T) : Bool {
        for (y in xs) {
            if (x == y) {
                return true;
            }
        }
        return false;
    }

    // Returns the index (zero based) of the first element of `xs` that 
    // satisfies `p`.
    // Returns -1 if no elements satisfy `p`.
    static public function firstIndexThat<T>(xs : Iterable<T>, p : T->Bool) : Int {
        var i = 0;
        for (x in xs) {
            if (p(x)) {
                return i;
            }
            ++i;
        }
        return -1;
    }

    // Returns the indices of the elements of the  iterator that satisfy the 
    // predicate.
    //todo: unit test.
    //todo: make lazy?
    static public function indicesThat<T>(iter : Iterable<T>, p : T->Bool) : Array<Int> {
        return indicesThatIterator(iter.iterator(), p);
    }
    static public function indicesThatIterator<T>(iter : Iterator<T>, p : T->Bool) : Array<Int> {
        var result = new Array<Int>();
        var i = 0;
        for (e in iter) {
            if (p(e)) {
                result.push(i);
            }
            ++i;
        }
        return result;
    }

    // Repeats `x` `n` times.
    static public function repeated<T>(x : T, n : Int) : Iterable<T> {
        return {iterator: function() return repeatedIterator(x, n)};
    }
    static public function repeatedIterator<T>(x : T, n : Int) : Iterator<T> {
        var i = 0;
        return {
            hasNext: function() return i < n,
            next: function() {
                ++i;
                return x;
            }
        };
    }

    // Repeats `x` indefinitely.
    static public function indefinitely<T>(x : T) : Iterable<T> {
        return {
            iterator: function() return {
                hasNext: function() return true,
                next: function() return x
            }
        };
    }

    // Yields the elements of first then the elements of second.
    // Lazy.
    static public function then<T>(first : Iterable<T>, second : Iterable<T>) : Iterable<T> {
        return {iterator: function() return thenIterator(first.iterator(), second.iterator())};
    }
    static public function thenIterator<T>(first : Iterator<T>, second : Iterator<T>) : Iterator<T> {
        return {
            hasNext: function() return first.hasNext() || second.hasNext(),
            next: function() return 
                if (first.hasNext()) first.next()
                else second.next()
        };
    }

    static public function then1<T>(
        first : Iterable<T>,
        second : T
    ) : Iterable<T> {
        return {
            iterator: function() {
                var firstIter = first.iterator();
                var hasSecond = true;
                return {
                    hasNext: function() return firstIter.hasNext() || hasSecond,
                    next: function() return
                        if (firstIter.hasNext()) firstIter.next()
                        else second
                }
            }
        }
    }
    // Converts an iterable of things into a comma (or something) seperated 
    // string using an element show function.
    static public function showElements<T>(iter : Iterable<T>, show : T->String, sep : String = ", ") : String {
        return showElementsIterator(iter.iterator(), show, sep);
    }
    static public function showElementsC<T>(show : T->String, sep : String = ", ") : Iterable<T>->String {
        return function(iter) return showElements(iter, show, sep);
    }
    static public function showElementsIterator<T>(iter : Iterator<T>, show : T->String, sep : String = ", ") : String {
        return interleavedFoldIterator(iter, new StringBuf(), 
            function(buf, elem) {
                buf.add(show(elem));
                return buf;
            },
            function(buf) {
                buf.add(sep);
                return buf;
            }
        ).toString();
    }

    static public function linifyElements<T>(
        as : Iterable<T>, 
        linifyElement : T->Int->String,
        indent : Int, 
        delimiter : String = ",",
        ind : String = "    "
    ) : String {
        var ind0 = "";
        for (i in 0...indent) {
            ind0 += ind;
        }
        var result = "";
        var first = true;
        for (a in as) {
            if (first) {
                first = false;
            } else {
                result += delimiter + "\n" + ind0;
            }
            result += linifyElement(a, indent);
        }
        return result;
    }

    // Linifies a list, `as`, with an element linifier, `linifyElement` 
    // interspaced with delimiters and wrapped in open and close brackets. 
    // Each element is shown on its own line and indented, e.g.: 
    // (
    //     e1,
    //     e2
    // )
    // The brackets are collapsed on a single line if the list is empty:
    // ()
    // Linifiers create strings of structured structures. A linifier must 
    // create lines with a hanging indent.
    static public function linifyList<T>(
        as : Iterable<T>, 
        linifyElement : T->Int->String,
        indent : Int, 
        open : String = "(", delimiter : String = ",", close = ")",
        ind : String = "    "
    ) : String {
        var ind0 = "";
        for (i in 0...indent) {
            ind0 += ind;
        }
        var ind1 = ind + ind0;
        var result = open;
        var first = true;
        for (a in as) {
            if (first) {
                first = false;
            } else {
                result += delimiter + "\n" + ind1;
            }
            result += linifyElement(a, indent + 1);
        }
        if (!first) {
            result += "\n" + ind0;
        }
        result += close;
        return result;
    }
    static public function linifyListC<T>(
        linifyElement : T->Int->String,
        open : String = "(", delimiter : String = ", ", close = ")",
        ind : String = "    "
    ) : Iterable<T>->Int->String {
        return function(as, indent) return 
            linifyList(as, linifyElement, indent, open, delimiter, close, ind);
    }

    static public function join(ss : Iterable<String>, sep : String) : String {
        return showElements(ss, function(s) return s, sep);
    }

    // Folds all elements in an iterable interleaved with a delimiter function. 
    // E.g. interleavedFold([e1, e2], a0, elem, delim) 
    // ==> elem(delim(elem(a0, a1)), a2)
    static public function interleavedFold<TE, TA>(
        iter : Iterable<TE>, a0 : TA, 
        onElem : TA->TE->TA, onDelim : TA->TA
    ) : TA {
        return interleavedFoldIterator(iter.iterator(), a0, onElem, onDelim);
    }
    static public function interleavedFoldIterator<TE, TA>(
        iter : Iterator<TE>, a0 : TA, 
        onElem : TA->TE->TA, onDelim : TA->TA
    ) : TA {
        var first = true;
        var a = a0;
        for (elem in iter) {
            if (first) {
                first = false;
            } else {
                a = onDelim(a);
            }
            a = onElem(a, elem);
        }
        return a;
    }

    // True iff xs and ys are equivalent as induced by an element equivalence.
    // I.e. xs, ys must have the same length and eq(xs[i], ys[i]) for all i in
    // range.
    //lazy (unfolds only as much as is needed)
    static public function eqBy<T1, T2>(xs : Iterable<T1>, ys : Iterable<T2>, eq : T1->T2->Bool) : Bool {
        return eqByIterator(xs.iterator(), ys.iterator(), eq);
    }
    static public function eqByIterator<T1, T2>(xs : Iterator<T1>, ys : Iterator<T2>, eq : T1->T2->Bool) : Bool {
        while (xs.hasNext() && ys.hasNext()) {
            if (!eq(xs.next(), ys.next())) {
                return false;
            }
        }
        return !xs.hasNext() && !ys.hasNext();
    }

    // Lazy variant of Lambda.map.
    static public function mapLazy<T1, T2>(xs : Iterable<T1>, f : T1->T2) : Iterable<T2> {
        return {iterator: function() return mapLazyIterator(xs.iterator(), f)};
    }
    static public function mapLazyIterator<T1, T2>(xs : Iterator<T1>, f : T1->T2) : Iterator<T2> {
        return {
            hasNext: xs.hasNext,
            next: function() return f(xs.next())
        };
    }
    static public function mapLazyC<T1, T2>(f : T1->T2) : Iterable<T1>->Iterable<T2> {
        return function(xs) return mapLazy(xs, f);
    }

    static public function zipKeyValue<T1, T2>(
        keys : Iterable<T1>, values : Iterable<T2>
    ) : Iterable<KeyValue<T1, T2>> {
        return zipWith(keys, values, function(k, v) return {key: k, value: v});
    }
    static public function zipWith<T1, T2, T>(
        as : Iterable<T1>, bs : Iterable<T2>,
        f : T1->T2->T
    ) : Iterable<T> return {
        iterator: function() return {
            var as = as.iterator();
            var bs = bs.iterator();
            return {
                hasNext: function() return as.hasNext() && bs.hasNext(),
                next: function() return f(as.next(), bs.next())
            };
        }
    }

    // Similar to `zipWith` but adds the trailing elements of the longer of 
    // `left` and `right`.
    // E.g. `mergeBy([1, 2], [3], add)` ==> `[4, 2]`
    static public function mergeBy<T>(
        left : Iterable<T>, right : Iterable<T>,
        merge : T->T->T
    ) : Iterable<T> {
        return mergeByGeneralized(left, right,
            function(v) return v,
            function(v) return v,
            merge
        );
    }
    static public function mergeByGeneralized<T1, T2, T>(
        left : Iterable<T1>, right : Iterable<T2>,
        injectLeft : T1->T, injectRight : T2->T, merge : T1->T2->T
    ) : Iterable<T> {
        return {
            iterator: function() return {
                var leftIter = left.iterator();
                var rightIter = right.iterator();
                {
                    hasNext: function() return leftIter.hasNext() || rightIter.hasNext(),
                    next: function() return {
                        if (leftIter.hasNext()) {
                            if (rightIter.hasNext()) {
                                merge(leftIter.next(), rightIter.next());
                            } else {
                                injectLeft(leftIter.next());
                            }
                        } else {
                            injectRight(rightIter.next());
                        }
                    }
                }
            }
        }
    }

    // Returns the elements of xs that are unique by getKey. Prefers earlier 
    // elements of xs over later. E.g.
    //   uniqueByString([5, 7, 15], function(i) return '${i % 5}')
    //   ==> [5, 7]
    //lazy
    static public function uniqueByString<T>(xs : Iterable<T>, getKey : T->String) : Iterable<T> {
        return {iterator: function() return uniqueByStringIterator(xs.iterator(), getKey)};
    }
    static public function uniqueByStringIterator<T>(xs : Iterator<T>, getKey : T->String) : Iterator<T> {
        var visited = new Map<String, String>();
        var ready = false;
        var has = false;
        var found = null;
        var advance = function() {
            has = false;
            while (xs.hasNext() && !has) {
                found = xs.next();
                var key = getKey(found);
                if (!visited.exists(key)) {
                    visited.set(key, key);
                    has = true;
                }
            }
            ready = true;
        };
        advance();
        return {
            hasNext: function() {
                if (!ready) {
                    advance();
                }
                return has;
            },
            next: function() {
                if (!ready) {
                    advance();
                }
                ready = false;
                return found;
            }
        };
    }

    static public function interleave<T>(xs : Iterable<T>, delim : T) : Iterable<T> {
        return { iterator: function() return interleaveIterator(xs.iterator(), delim) };
    }
    static public function interleaveIterator<T>(xs : Iterator<T>, delim : T) : Iterator<T> {
        var isDelim = false;
        return {
            hasNext: function() return isDelim || xs.hasNext(),
            next: function() return 
                if (isDelim) {
                    isDelim = false;
                    delim;
                } else {
                    var result = xs.next();
                    isDelim = xs.hasNext();
                    result;
                }
        };
    }

    static public function withIndices<T>(xs : Iterable<T>) : Iterable<{i : Int, value : T}> {
        return { iterator: function() return withIndicesIterator(xs.iterator()) };
    }
    static public function withIndicesIterator<T>(xs : Iterator<T>) : Iterator<{i : Int, value : T}> {
        var i = 0;
        return {
            hasNext: function() return xs.hasNext(),
            next: function() {
                var result = { i: i, value: xs.next() };
                ++i;
                return result;
            }
        };
    }

    // Compares two sequences lexicographically. E.g. 
    //   [] < ["hello"]
    //   ["hello"] < ["world"]
    //   ["hello"] < ["hello", "world"]
    //todo: unit test
    static public function lexicographicalCompareBy<T1, T2>(xs : Iterable<T1>, ys : Iterable<T2>, compare : T1->T2->Int) : Int {
        return lexicographicalCompareByIterator(xs.iterator(), ys.iterator(), compare);
    }
    static public function lexicographicalCompareByIterator<T1, T2>(xs : Iterator<T1>, ys : Iterator<T2>, compare : T1->T2->Int) : Int {
        while (xs.hasNext() && ys.hasNext()) {
            var order = compare(xs.next(), ys.next());
            if (order != 0) {
                return order;
            }
        }
        return if (xs.hasNext()) 1
        else if (ys.hasNext()) -1
        else 0;
    }

}
