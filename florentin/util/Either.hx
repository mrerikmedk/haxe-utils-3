package florentin.util;

enum Either<T1, T2> {
    Inl(v : T1);
    Inr(v : T2);
}

class EitherUtils {

    static public function show<T1, T2>(
        e : Either<T1, T2>,
        showLeft : T1->String,
        showRight : T2->String
    ) : String {
        return switch (e) {
            case Inl(v): 'inl(${showLeft(v)})';
            case Inr(v): 'inr(${showRight(v)})';
        }
    }

    static public function isInl<T1, T2>(
        e : Either<T1, T2>
    ) : Bool {
        return switch (e) {
            case Inl(_): true;
            case Inr(_): false;
        }
    }
    static public function isInr<T1, T2>(
        e : Either<T1, T2>
    ) : Bool {
        return switch (e) {
            case Inl(_): false;
            case Inr(_): true;
        }
    }

    static public function getL<T1, T2>(
        e : Either<T1, T2>
    ) : T1 {
        return switch (e) {
            case Inl(v): v;
            case Inr(_): throw("Not an Inl");
        }
    }
    static public function getR<T1, T2>(
        e : Either<T1, T2>
    ) : T2 {
        return switch (e) {
            case Inr(v): v;
            case Inl(_): throw("Not an Inr");
        }
    }
}
