package florentin.util;

using StringTools;
using florentin.util.StringLike;
using florentin.util.Substring;
using florentin.util.detail.StringCodeIterator;

// Utility functions over strings.
class StringUtils {

    // Repeats the same string n times. E.g.
    //   repeat("hello", 3) ==> "hellohellohello"
    static public function repeat(s : String, n : Int) : String {
        var buf = new StringBuf();
        for (i in 0...n) {
            buf.add(s);
        }
        return buf.toString();
    }

    // Reverses a string. 
    static public function reverse(s : String) : String {
        // People say this is the most efficient way to reverse strings in
        // Haxe (facepalm).
        var cs = s.split('');
        cs.reverse();
        return cs.join('');
    }

    // Adds parentheses around s if cond.
    static public function parenthesize(s : String, cond : Bool) : String {
        return if (cond) '($s)' else s;
    }

    // True iff s starts with prefix.
    //   startsWith("Hello world", "Hello") ==> true
    //   startsWith("Hello world", "bonjour") ==> false
    static public function startsWith(s : String, prefix : String) : Bool {
        for (i in 0...prefix.length) {
            if (i >= s.length || s.fastCodeAt(i) != prefix.fastCodeAt(i)) {
                return false;
            }
        }
        return true;
    }

    static public function substringAt(s : String, i : Int, substring : String) : Bool {
        for (j in 0...substring.length) {
            if (j + i >= s.length || s.fastCodeAt(i + j) != substring.fastCodeAt(j)) {
                return false;
            }
        }
        return true;
    }

    // Gets a substring of `s` from `i` (including) to `i + n` (excluding). 
    static public function substring(s : String, i : Int, n : Int) : Substring {
        return if (0 <= i && 0 <= n && i + n < s.length) {
            s: s, i: i, n: n
        } else throw "Indices out of range.";
    }

    static public function sequence(s : String) : StringLike {
        return {
            length: s.length,
            get: function(i) return s.fastCodeAt(i)
        };
    }

    // Limits the length of s to max l characters. If s is longer than l, the 
    // tail of the string is removed and ellipsis is appended to make the 
    // string l characters long.
    static public function limit(s : String, l : Int, ellipsis : String = "...") {
        return limitFrom(s, 0, l, ellipsis);
    }

    // Equivalent to limit(s.substr(i), l, ellipses) but maybe more efficient.
    static public function limitFrom(s : String, i : Int, l : Int, ellipsis : String = "...") {
        if (s.length > l + i) {
            if (ellipsis.length > l) {
                return ellipsis.substr(0, l);
            } else {
                return s.substr(i, l - ellipsis.length) + ellipsis;
            }
        } else {
            if (i != 0) {
                return s.substr(i);
            } else {
                return s;
            }
        }
    }

    // Reverse version of limit. I.e. it removes characters from the start of 
    // the string instead of the back.
    static public function limitBack(s : String, n : Int, ellipsis : String = "...") : String {
        return limitBackFrom(s, s.length, n, ellipsis);
    }
    // Reverse version of limitFrom. The i indicates the index of the end of 
    // the desired segment.
    static public function limitBackFrom(s : String, i : Int, n : Int, ellipsis : String = "...") : String {
        if (i > n) {
            if (ellipsis.length > n) {
                return ellipsis.substr(ellipsis.length - n);
            } else {
                return ellipsis + s.substr(i - n + ellipsis.length, n - ellipsis.length);
            }
        } else {
            if (i != s.length) {
                return s.substr(0, i);
            } else {
                return s;
            }
        }
    }

    // Shows a fragment of the line at index i in s. Useful for parser error 
    // messages. Returns at most n characters. Truncates with the ellipsis.
    // Returns "<eof>" if i points to end-of-file, or "<eol>" if i points to 
    // an end-of-line.
    //todo: unit test
    static public function lineFragment(s : String, i : Int, n : Int, ellipsis = "...") : String {
        return if (i >= s.length) "<eof>"
        else {
            var segment = limit(getLine(s, i), n);
            if (segment.length > 0) segment else "<eol>";
        }
    }

    // Splits s into lines. Returns one empty line for an empty s. If s ends 
    // with a new line, the result will end with an empty line.
    //todo: unit test
    static public function splitLines(s : String) : Array<String> {
        var result = new Array<String>();
        var i0 = 0;
        var i = indexOfEol(s);
        while (i < s.length) {
            result.push(s.substr(i0, i - i0));
            i0 = i + eolSize(s, i);
            i = indexOfEol(s, i0);
        }
        result.push(s.substr(i0, i - i0));
        return result;
    }

    // Gets the first line of s starting at i0.
    static public function getLine(s : String, i0 : Int = 0) : String {
        var i = indexOfEol(s, i0);
        return s.substr(i0, i - i0);
    }

    // Returns the index of the first end of line marker at or after i0. Eol 
    // is indicated by "\r", "\n" or eof.
    static public function indexOfEol(s : String, i0 : Int = 0) : Int {
        var r = s.indexOf("\r", i0);
        var n = s.indexOf("\n", i0);
        return if (r >= 0)
            if (n >= 0)
                if (r < n) r else n
            else r
        else 
            if (n >= 0) n else s.length;
    }

    // The size of the end of line marker at i. Recognizes "\n", "\r", "\n\r",
    // "\r\n", and eof as end of line markers. Returns 1, 1, 2, 2, resp. 0. 
    // Returns -1 if i does not point to an end of line marker.
    static public function eolSize(s : String, i : Int) : Int {
        return if (i < s.length) switch (s.fastCodeAt(i)) {
            case "\r".code: if (i + 1 < s.length && s.fastCodeAt(i + 1) == "\n".code) 2 else 1;
            case "\n".code: if (i + 1 < s.length && s.fastCodeAt(i + 1) == "\r".code) 2 else 1;
            default: -1;
        } else 0;
    }

    // Iterates over all character codes in the string in order.
    static public function codes(s : String) : Iterable<Int> {
        return {iterator: function() return codesIterator(s) };
    }
    static public function codesIterator(s : String) : Iterator<Int> {
        return new StringCodeIterator(s);
    }

    
}