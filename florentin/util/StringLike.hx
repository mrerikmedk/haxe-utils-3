package florentin.util;

using String;
using florentin.util.Sequence;
using florentin.util.StringUtils;

typedef StringLike = Sequence<Int>;

class StringLikeUtils {

    static public function show(s : StringLike) : String {
        var s = "";
        for (c in s.codes()) {
            s += c.fromCharCode();
        }
        return s;
    }

    // Limits the length of s to max l characters. If s is longer than l, the 
    // tail of the string is removed and ellipsis is appended to make the 
    // string l characters long.
    static public function limit(s : StringLike, l : Int, ellipsis : String = "...") {
        return limitFrom(s, 0, l, ellipsis);
    }

    // Equivalent to limit(s.substr(i), l, ellipses) but maybe more efficient.
    static public function limitFrom(s : StringLike, i : Int, l : Int, ellipsis : String = "...") {
        if (s.length > l + i) {
            if (ellipsis.length > l) {
                return ellipsis.substr(0, l).sequence();
            } else {
                return s.sub(i, l - ellipsis.length)
                    .then(ellipsis.sequence());
            }
        } else {
            if (i != 0) {
                return s.drop(i);
            } else {
                return s;
            }
        }
    }

    static public function cacheString(s : StringLike) : StringLike {
        return show(s).sequence();
    }

}
