package florentin.util;

using haxe.PosInfos;

class FunUtils {

    static public function id<T>(x : T) : T {
        return x;
    }

    static public function assert(cond : Bool, message : String, ?pos : PosInfos) : Void {
        if (!cond) {
            throw '${pos.fileName}(${pos.lineNumber}) : Assert failed: $message';
        }
    }

}

