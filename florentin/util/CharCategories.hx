package florentin.util;

// Common categorizations for character-codes.
class CharCategories {
    
    // True iff c is a digit character (0-9).
    // todo: unit test
    static public function isDigit(c : Int) : Bool {
        return "0".code <= c && c <= "9".code;
    }

    // True iff c can be the first character of a common identifier 
    // (a-zA-Z or _)
    // todo: unit test
    static public function isInitNameChar(c : Int) : Bool {
        return "a".code <= c && c <= "z".code
            || "A".code <= c && c <= "Z".code
            || c == "_".code;
    }

    // True iff c can be a body character of a common identifier 
    // (a-zA-Z0-9 or _)    
    // todo: unit test
    static public function isNameChar(c : Int) : Bool {
        return "0".code <= c && c <= "9".code
            || "a".code <= c && c <= "z".code
            || "A".code <= c && c <= "Z".code
            || c == "_".code;
    }

    // True iff c is a lower case ascii letter (a-z).
    // todo: unit test
    static public function isLower(c : Int) : Bool {
        return "a".code <= c && c <= "z".code;
    }

    // True iff c is a lower case ascii letter (a-z).
    // todo: unit test
    static public function isUpper(c : Int) : Bool {
        return "A".code <= c && c <= "Z".code;
    }

    static public function isAlpha(c : Int) : Bool {
        return "a".code <= c && c <= "z".code
            || "A".code <= c && c <= "Z".code;
    }

    static public function isAlphanum(c : Int) : Bool {
        return "0".code <= c && c <= "9".code
            || "a".code <= c && c <= "z".code
            || "A".code <= c && c <= "Z".code;
    }

    // True iff c is a base b digit.
    static public function isBaseChar(c : Int, b : Int) : Bool {
        var i = fromBaseChar(c);
        return 0 <= i && i < b;
    }

    // Returns the digit value of c in a base <= 2?.
    static public function fromBaseChar(c : Int) : Int {
        return if ("0".code <= c && c <= "9".code) c - "0".code;
        else if ("a".code <= c && c <= "z".code) c - "a".code + 10;
        else if ("A".code <= c && c <= "Z".code) c - "A".code + 10;
        else -1;
    }

    static public function isSpace(c : Int) : Bool {
        return c == " ".code || c == "\t".code 
            || c == "\n".code || c == "\r".code;
    }
    static public function isHSpace(c : Int) : Bool {
        return c == " ".code || c == "\t".code;
    }
    static public function isVSpace(c : Int) : Bool {
        return c == "\n".code || c == "\r".code;
    }
}