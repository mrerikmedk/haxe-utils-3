package florentin.util;

using haxe.PosInfos;
using haxe.unit.TestCase;
using florentin.util.MathUtils;

class UnitTestUtils {
    
    // Asserts that actual is equivalent by eq to expected.
    static public function assertEqBy<T1, T2>(test : TestCase,
        expected : T1, actual : T2,
        eq : T1->T2->Bool,
        ?pos : PosInfos
    ) : Void {
        assertEqByShow(test, expected, actual, eq, function(x) return '$x', function(y) return '$y');
    }

    // Asserts that actual is equivalent by eq to expected. Takes custom show 
    // functions for expected and actual.
    static public function assertEqByShow<T1, T2>(test : TestCase,
        expected : T1, actual : T2,
        eq : T1->T2->Bool,
        showExpected : T1->String, showActual : T2->String, 
        ?pos : PosInfos
    ) : Void {
        test.currentTest.done = true;
        if (!eq(expected, actual)) {
            failWith(test, 'Actual value ${showActual(actual)} is not equivalent to expected value ${showExpected(expected)}.', pos);
        }
    }

    // Asserts that actual is not equivalent by eq to expected.
    static public function assertNeqBy<T1, T2>(test : TestCase,
        expected : T1, actual : T2,
        eq : T1->T2->Bool,
        ?pos : PosInfos
    ) : Void {
        test.currentTest.done = true;
        if (eq(expected, actual)) {
            failWith(test, 'Actual value $actual is not equivalent to expected value $expected.', pos);
        }
    }

    static public function assertLtBy<T1, T2>(test : TestCase,
        expected : T1, actual : T2,
        lt : T1->T2->Bool,
        ?pos : PosInfos
    ) : Void {
        test.currentTest.done = true;
        if (!lt(expected, actual)) {
            failWith(test, 'Expected value $expected is not less than actual value $actual.', pos);
        }
    }

    static public function assertNltBy<T1, T2>(test : TestCase,
        expected : T1, actual : T2,
        lt : T1->T2->Bool,
        ?pos : PosInfos
    ) : Void {
        test.currentTest.done = true;
        if (lt(expected, actual)) {
            failWith(test, 'Expected value $expected is less than actual value $actual.', pos);
        }
    }

    // Fails the unit test if two floating point numbers are not sufficiently 
    // close.
    static public function assertClose(test : TestCase, 
        expected : Float, actual : Float, 
        ?pos : PosInfos
    ) : Void {
        test.currentTest.done = true;
        if (!expected.close(actual)) {
            failWith(test, 'Numbers are not sufficiently equal: $actual != $expected', pos);
        }
    }

    // Fails a unit test case with the given message.
    static public function failWith(test : TestCase, message : String, pos : PosInfos) {
        test.currentTest.error = message;
        test.currentTest.posInfos = pos;
        test.currentTest.success = false;
        throw test.currentTest;
    }
    
}