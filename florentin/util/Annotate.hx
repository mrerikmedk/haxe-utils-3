package florentin.util;

// Annotates a value with some meta info.
typedef Annotate<TV, TI> = {
    value : TV,
    info : TI
}
typedef StringWith<TI> = Annotate<String, TI>;

class AnnotateUtils {

    // Creates an annotated value.
    static public function annotate<TV, TI>(v : TV, i : TI) : Annotate<TV, TI> {
        return {value: v, info: i};
    }

    static public function map<TV1, TV2, TI>(
        a : Annotate<TV1, TI>,
        f : TV1->TV2
    ) : Annotate<TV2, TI> {
        return {value: f(a.value), info: a.info};
    }

    // Applies a function to the annotated info.
    static public function mapInfo<TV, TI1, TI2>(
        a : Annotate<TV, TI1>,
        f : TI1->TI2
    ) : Annotate<TV, TI2> {
        return {value: a.value, info: f(a.info)};
    }
    static public function mapInfoC<TV, TI1, TI2>(
        f : TI1->TI2
    ) : Annotate<TV, TI1>->Annotate<TV, TI2> {
        return function(a) return mapInfo(a, f);
    }

    
}

