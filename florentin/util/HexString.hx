package florentin.util;

using florentin.util.StringUtils;
using florentin.util.CharUtils;

class HexString {
    static public function toBase(value : Int, b : Int) : String {
        return if (value == 0) "0"
        else if (value > 0) {
            var work = value;
            var buf = new StringBuf();
            while (work > 0) {
                var digit = work % b;
                work = Std.int(work / b);
                buf.addChar(digit.toBaseDigit());
            }
            buf.toString().reverse();
        } else "?";
    }

    // Converts value to base b left padding with zeros to fill w characters.
    // If values character representation is wider than w, only the last w
    // characters of the representation is shown:
    //   toBaseField(15, 16, 2) ==> "0F"
    //   toBaseField(117, 10, 2) ==> "17"
    static public function toBaseField(value : Int, b : Int, w : Int) : String {
        var work = value;
        var buf = new StringBuf();
        for (i in 0...w) {
            var digit = work % b;
            work = Std.int(work / b);
            buf.addChar(digit.toBaseDigit());
        }
        return buf.toString().reverse();
    }
    // Curried version of toBaseField
    static public function toBaseFieldC(b : Int, w : Int) : Int->String {
        return function (value : Int) {
            return toBaseField(value, b, w);
        };
    }

}