package florentin.util;

using StringTools;
using haxe.io.Bytes;
using florentin.util.StringUtils;

class ShowBinary {
    // Shows binary data as a number of lines with 8-bit hex codes and 
    // (printable) characters.
    // Does not indent the first line and does not add a trailing newline.
    static public function showHexLines(b : Bytes, indent : Int) : String {
        var ind = "  ".repeat(indent);
        var buf = new StringBuf();
        var row = 0;
        var first = true;
        while (row < b.length) {
            if (first) {
                first = false;
            } else {
                buf.add('\n$ind');
            }
            buf.add('${row.hex(8)}:');
            for (d in 0...16) {
                var i = row + d;
                var s = if (i < b.length) b.get(i).hex(2) else "  ";
                buf.add(if (d != 8) ' $s' else '  $s');
            }
            buf.add(" |");
            for (d in 0...16) {
                var i = row + d;
                var c = if (i < b.length) makePrintable(b.get(i)) else " ".code;
                if (d == 8) {
                    buf.add(" ");
                }
                buf.addChar(c);
            }
            buf.add("|");
            row += 16;
        }
        return buf.toString();
    }

    // Returns "." iff c is not (8-bit) printable. Otherwise c.
    static public function makePrintable(c : Int) : Int {
        return if (printable(c)) c else ".".code;
    }

    // True iff c represents an 8-bit printable character.
    static public function printable(c : Int) : Bool {
        return 0x20 <= c && c <= 0x7E;
    }    
}