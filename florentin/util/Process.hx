package florentin.util;

using Lambda;
import haxe.Timer;
import florentin.util.Failable;

typedef Process<T> = ProcessBy<T, String>;
enum ProcessBy<T, TE> {
    Resolved(v : T);
    Failed(e : TE);
    Async(f : AsyncBy<T, TE>);
}
typedef AsyncBy<T, TE> = (T->Void)->(TE->Void)->Void;


class ProcessUtils {
    static public function resolved<T, TE>(
        v : T
    ) : ProcessBy<T, TE> {
        return Resolved(v);
    }
    static public function failed<T, TE>(
        e : TE
    ) : ProcessBy<T, TE> {
        return Failed(e);
    }
    static public function delay<T, TE>(
        v : T, ms : Int
    ) : ProcessBy<T, TE> {
        return Async(function(onResolve, onFail) Timer.delay(function()
            onResolve(v), ms
        ));
    }
    static public function delayFail<T, TE>(
        e : TE, ms : Int
    ) : ProcessBy<T, TE> {
        return Async(function(onResolve, onFail) Timer.delay(function()
            onFail(e), ms
        ));
    }

    static public function mbind<T1, T2, TE>(
        p1 : ProcessBy<T1, TE>,
        p2 : T1->ProcessBy<T2, TE>
    ) : ProcessBy<T2, TE> {
        return switch (p1) {
            case Resolved(v1): p2(v1);
            case Failed(e1): Failed(e1);
            case Async(f1): Async(function(onResolve, onFail) {
                function onResolve1(v1) switch (p2(v1)) {
                    case Resolved(v2): onResolve(v2);
                    case Failed(e2): onFail(e2);
                    case Async(f2): f2(onResolve, onFail);
                }
                f1(onResolve1, onFail);
            });
        }
    }

    static public function mmap<T1, T2, TE>(
        p : ProcessBy<T1, TE>,
        f : T1->T2
    ) : ProcessBy<T2, TE> {
        return mbind(p, function(v) return Resolved(f(v)));
    }

    static public function andCombine<T1, T2, T3, TE>(
        p1 : ProcessBy<T1, TE>,
        p2 : ProcessBy<T2, TE>,
        combine : T1->T2->T3
    ) : ProcessBy<T3, TE> {
        return switch (p1) {
            case Resolved(v1): switch (p2) {
                case Resolved(v2): Resolved(combine(v1, v2));
                case Failed(e2): Failed(e2);
                case Async(f2): Async(function(onResolve, onFail)
                    f2(function(v2) onResolve(combine(v1, v2)), onFail)
                );
            }
            case Failed(e1): Failed(e1);
            case Async(f1): switch (p2) {
                case Resolved(v2): Async(function(onResolve, onFail)
                    f1(function(v1) onResolve(combine(v1, v2)), onFail)
                );
                case Failed(e2): Failed(e2);
                case Async(f2): Async(function(onResolve, onFail) {
                    var done1 = false;
                    var done2 = false;
                    var error = false;
                    var value1 = null;
                    var value2 = null;
                    function onResolve1(v1) {
                        if (done1) throw "multiple dones";
                        if (!error) {
                            if (done2) {
                                onResolve(combine(v1, value2));
                            } else {
                                value1 = v1;
                                done1 = true;
                            }
                        }
                    }
                    function onResolve2(v2) {
                        if (done2) throw "multiple dones";
                        if (!error) {
                            if (done1) {
                                onResolve(combine(value1, v2));
                            } else {
                                value2 = v2;
                                done2 = true;
                            }
                        }
                    }
                    function onFail3(e) {
                        if (!error) {
                            error = true;
                            onFail(e);
                        }
                    }
                    f1(onResolve1, onFail3);
                    f2(onResolve2, onFail3);
                });
            }
        }
    }
    static public function ands<T, TE>(
        ps : Array<ProcessBy<T, TE>>
    ) : ProcessBy<Array<T>, TE> {
        //TODO: optimize
        return ps.fold(function(p : ProcessBy<T, TE>, p0 : ProcessBy<Array<T>, TE>) : ProcessBy<Array<T>, TE> return 
            andCombine(p0, p, function(vs, v) return 
                vs.concat([v])
            ),
            resolved([])
        );
    }

    static public function handle<T, TE>(
        p : ProcessBy<T, TE>,
        handle : TE->ProcessBy<T, TE>
    ) : ProcessBy<T, TE> {
        return switch (p) {
            case Resolved(v): Resolved(v);
            case Failed(err): handle(err);
            case Async(f):
                Async(function(onDone, onError) return {
                    function onError1(err) return switch (handle(err)) {
                        case Resolved(v1): onDone(v1);
                        case Failed(err1): onError(err1);
                        case Async(f1): f1(onDone, onError);
                    }
                    f(onDone, onError1);
                });
        }
    }

    static public function run<T, TE>(
        p : ProcessBy<T, TE>
    ) : Void {
        await(p, function(_) {}, function(msg) trace(msg));
    }
    static public function await<T, TE>(
        p : ProcessBy<T, TE>,
        onResolve : T->Void,
        onFail : TE->Void
    ) : Void {
        return switch (p) {
            case Resolved(v): onResolve(v);
            case Failed(e): onFail(e);
            case Async(f): f(onResolve, onFail);
        }
    }

    static public function cbToProcess<T, TE>(
        f : (T->Void)->(TE->Void)->Void
    ) : ProcessBy<T, TE> {
        return Async((onResolved, onFailed)->{
            var sync = true;
            function onResolved2(v : T) {
                if (sync) await(delay(v, 0), onResolved, onFailed)
                else onResolved(v);
            }
            function onFailed2(error : TE) {
                if (sync) await(delayFail(error, 0), onResolved, onFailed)
                else onFailed(error);
            }
            f(onResolved2, onFailed2);
            sync = false;
        });

    }

    static public function failableToProcess<T, TE>(
        f : FailableBy<T, TE>
    ) : ProcessBy<T, TE> {
        return switch (f) {
            case Ok(v): Resolved(v);
            case Failed(e): Failed(e);
        }
    }


}