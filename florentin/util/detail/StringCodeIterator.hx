package florentin.util.detail;

class StringCodeIterator {
    public function new(s : String) {
        this.s = s;
        this.i = 0;
    }
    public function hasNext() : Bool {
        return i < s.length;
    }
    public function next() : Int {
        return s.charCodeAt(i++);
    }
    private var s : String;
    private var i : Int;
}
