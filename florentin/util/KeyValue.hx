package florentin.util;

import florentin.util.FunUtils;
import florentin.util.Lens;

typedef KeyValue<TK, TV> = {key : TK, value : TV};

class KeyValueUtils {
    static public function is<TK, TV>(key : TK, value : TV) : KeyValue<TK, TV> {
        return {key: key, value: value};
    }
    
    static public function show<TK, TV>(
        pair : KeyValue<TK, TV>,
        showKey : TK->String, showValue : TV->String
    ) : String {
        return '${showKey(pair.key)}=${showValue(pair.value)}';
    }
    static public function showC<TK, TV>(
        showKey : TK->String, showValue : TV->String
    ) : KeyValue<TK, TV>->String {
        return function(pair) return show(pair, showKey, showValue);
    }

    static public function map<TK1, TV1, TK2, TV2>(
        pair : KeyValue<TK1, TV1>,
        fKey : TK1->TK2, fValue : TV1->TV2
    ) : KeyValue<TK2, TV2> {
        return {key: fKey(pair.key), value: fValue(pair.value)};
    }
    static public function mapC<TK1, TV1, TK2, TV2>(
        fKey : TK1->TK2, fValue : TV1->TV2
    ) : KeyValue<TK1, TV1>->KeyValue<TK2, TV2> {
        return function(pair) return map(pair, fKey, fValue);
    }

    static public function mapKey<TK1, TK2, TV>(
        pair : KeyValue<TK1, TV>,
        fKey : TK1->TK2
    ) : KeyValue<TK2, TV> {
        return map(pair, fKey, FunUtils.id);
    }
    static public function mapKeyC<TK1, TK2, TV>(
        fKey : TK1->TK2
    ) : KeyValue<TK1, TV>->KeyValue<TK2, TV> {
        return function(pair) return mapKey(pair, fKey);
    }

    static public function mapValue<TK, TV1, TV2>(
        pair : KeyValue<TK, TV1>,
        fValue : TV1->TV2
    ) : KeyValue<TK, TV2> {
        return map(pair, FunUtils.id, fValue);
    }
    static public function mapValueC<TK, TV1, TV2>(
        fValue : TV1->TV2
    ) : KeyValue<TK, TV1>->KeyValue<TK, TV2> {
        return function(pair) return mapValue(pair, fValue);
    }

    static public function lKey<TK1, TK2, TV>() : LensGeneralized<KeyValue<TK1, TV>, TK1, KeyValue<TK2, TV>, TK2> {
        return {
            get: function(p) return p.key,
            set: function(p, k) return {key: k, value: p.value}
        };
    }
    static public function lValue<TK, TV1, TV2>() : LensGeneralized<KeyValue<TK, TV1>, TV1, KeyValue<TK, TV2>, TV2> {
        return {
            get: function(p) return p.value,
            set: function(p, v) return {key: p.key, value: v}
        };
    }
}