package florentin.util;

using Lambda;
using florentin.util.IteratorUtils;
using florentin.util.KeyValue;
using florentin.util.MapUtils;

// A repmap is a map where each key is mapped to a sequence of values.
class Repmap<TK, TV> {
    public function new(
        map : Map<TK, Array<TV>>
    ) {
        this.map = map;
    }

    public function insert(key : TK, value : TV) : Repmap<TK, TV> {
        var map1 = copy();
        map1.insertMutate(key, value);
        return map1;
    }
    public function inserts(pairs : Iterable<KeyValue<TK, TV>>) : Repmap<TK, TV> {
        var map1 = copy();
        for (pair in pairs) {
            map1.insertMutate(pair.key, pair.value);
        }
        return map1;
    }

    public function get(key : TK) : Array<TV> {
        return if (map.exists(key)) map.get(key)
        else [];
    }

    public function keys() : Iterable<TK> {
        return map.keys2();
    }
    public function pairs() : Iterable<KeyValue<TK, Array<TV>>> {
        return keys().mapLazy(key->key.is(map.get(key)));
    }

    public function copy() : Repmap<TK, TV> {
        var map1 = map.copy();
        for (key in map1.keys()) {
            map1.set(key, map1.get(key).array());
        }
        return new Repmap(map1);
    }
    public function insertMutate(key : TK, value : TV) {
        var vs = map.get(key);
        if (vs != null) {
            vs.push(value);
        } else {
            map.set(key, [value]);
        }
    }

    private var map : Map<TK, Array<TV>>;
}

class RepmapUtils {
    static public function emptyString<T>() : Repmap<String, T> {
        return new Repmap<String, T>(new Map());
    }
    static public function show<TK, TV>(
        map : Repmap<TK, TV>,
        showKey : TK->String,
        showValue : TV->String,
        mapsTo : String = ": ",
        delimiterMap : String = ", ",
        delimiterSeq : String = ", ",
        bracesMap : String->String = null,
        bracesSeq : String->String = null
    ) : String {
        if (bracesMap == null) {
            bracesMap = s->"{" + s + "}";
        }
        if (bracesSeq == null) {
            bracesSeq = s->"[" + s + "]";
        }
        return bracesMap(map.pairs().showElements(
            pair->showKey(pair.key) + mapsTo 
          + bracesSeq(pair.value.showElements(showValue, delimiterSeq)),
            delimiterMap
        ));
    }
}