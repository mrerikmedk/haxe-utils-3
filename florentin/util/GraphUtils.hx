package florentin.util;

class GraphUtils {

    // Primitive brute force graph coloring. Accepts a number of nodes to be 
    // colored and a function that gives graph neighbors. Returns a map 
    // associating nodes with colors as well as the number of colors used.
    static public function colorGreedyString(
        nodes : Iterable<String>,
        getNeighbors : String->Iterable<String>
    ) : {colors : Map<String, Int>, nColors : Int} {
        var result = new Map<String, Int>();
        var nColors : Int = 0;
        for (node in nodes) {
            var neighborColors = new Map<Int, Int>();
            var neighbors = getNeighbors(node);
            for (neighbor in neighbors) {
                var neighborColor = result.get(neighbor);
                if (neighborColor != null) {
                    neighborColors.set(neighborColor, neighborColor);
                }
            }

            var color = 0;
            while (neighborColors.exists(color)) {
                ++color;
            }
            result.set(node, color);
            if (color >= nColors) {
                nColors = color + 1;
            }
        }
        return {colors: result, nColors: nColors};
    }

}
