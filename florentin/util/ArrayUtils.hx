package florentin.util;

using Lambda;

class ArrayUtils {

    static public function tryGet<T>(xs : Array<T>, i : Int) : Null<T> {
        return if (0 <= i && i < xs.length) xs[i] else null;
    }

    static public function sequence<T>(xs : Array<T>) : Sequence<T> {
        return {
            length: xs.length,
            get: function(i) return xs[i]
        };
    }

    // Iterates through as in reverse.
    static public function riterate<T>(
        as : Array<T>
    ) : Iterable<T> {
        return {
            iterator: function() return {
                var i = 0;
                {
                    hasNext: function() return i < as.length,
                    next: function() return {
                        var a = as[as.length - i - 1];
                        ++i;
                        a;
                    }
                }
            }
        }
    }

    // True iff as is a permutation of bs (by element equality induced by 
    // `==`).
    // Quadratic time.
    static public function isPermutationSlow<T>(
        as : Array<T>, bs : Array<T>
    ) : Bool {
        return isPermutationBySlow(as, bs, function(a, b) return a == b);
    }

    // True iff `as` is a permutation of `bs` (by element equality `eq`).
    // Quadratic time.
    static public function isPermutationBySlow<T1, T2>(
        as : Array<T1>, bs : Array<T2>,
        eq : T1->T2->Bool
    ) : Bool {
        var visited : Array<Bool> = bs.map(function(_) return false);
        var j0 = 0;
        for (a in as) {
            var j = j0;
            var found = false;
            while (j < bs.length && !found) {
                if (!visited[j] && eq(a, bs[j])) {
                    visited[j] = true;
                    found = true;
                    while (visited[j0]) {
                        ++j0;
                    }
                } else {
                    ++j;
                }
            }
            if (!found) {
                return false;
            }
        }
        return visited.foreach(function(v) return v);
    }

    // Sets the `i`th element of `xs` to `x` filling with `fill` if necessary.
    static public function putAtFill<T>(
        xs : Array<T>, i : Int, x : T, fill : T
    ) : Array<T> {
        var ys = xs.array();
        setAtFill(ys, i, x, fill);
        return ys;
    }

    // Sets the `i`th element of `xs` to `x` filling with `x` if necessary.
    // Mutates `xs`.
    static public function setAtExtend<T>(
        xs : Array<T>, i : Int, x : T
    ) : Void {
        return setAtFill(xs, i, x, x);
    }

    // Sets the `i`th element of `xs` to `x` filling with `fill` if necessary.
    // Mutates `xs`.
    static public function setAtFill<T>(
        xs : Array<T>, i : Int, x : T, fill : T
    ) : Void {
        if (0 <= i) {
            while (xs.length < i + 1) {
                xs.push(fill);
            }
            xs[i] = x;
        } else {
            throw "index out of range.";
        }


    }

    // Partitions a sorted array into less-than and greater-than-or-equal.
    // Finds the first index into a monotoneously increasting `as` where `ge` 
    // holds.
    // Returns 0 if all elements are greater-than-or-equal. Returns 
    // `as.length` if all elements are less-than.
    static public function binaryPartition<T>(
        as : Array<T>,
        ge: T->Bool
    ) : Int {
        var i = 0;
        var j = as.length;
        while (i < j) {
            var k = Std.int((i + j) / 2);
            if (ge(as[k])) {
                j = k;
            } else {
                i = k + 1;
            }
        }
        return i;
        

    }
}
