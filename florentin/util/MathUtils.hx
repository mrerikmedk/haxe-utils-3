package florentin.util;

using Array;
using florentin.util.Either;
using florentin.util.IteratorUtils;

class MathUtils {

    // True iff v1 and v2 are pretty close (i.e. distance is less than a 
    // millionth of the magnitude of v1 or v2).
    //todo: unit test.
    static public function close(v1 : Float, v2 : Float) : Bool {
        if (v1 == 0 && v2 == 0) {
            return true;
        } else {
            var a1 = Math.abs(v1);
            var a2 = Math.abs(v2);
            return if (a1 >= a2) Math.abs(v1 - v2) / a1 < 1e-6
            else Math.abs(v1 - v2) / a2 < 1e-6;
        }
    }

    static public function imin(v1 : Int, v2 : Int) : Int {
        return if (v1 < v2) v1 else v2;
    }
    static public function imax(v1 : Int, v2 : Int) : Int {
        return if (v1 > v2) v1 else v2;
    }

    static public function topologicalSort(
        vs : Iterable<String>,
        es : String->Iterable<String>,
        logError : String->Void
    ) : Null<Array<String>> {
        return switch (tarjanTopologicalSort(vs, es)) {
            case Inr(vs): vs;
            case Inl(cs):
                var csString = cs.showElements(c->
                    "(" + c.showElements(v->v, ", ") + ")",
                    ", "
                );
                logError('Cannot topologcal sort graph as it has strongly connected component(s) $csString.');
                null;
        }
    }
    static public function tarjanTopologicalSort(
        vs : Iterable<String>,
        es : String->Iterable<String>
    ) : Either<Array<Array<String>>, Array<String>> {
        var cs = tarjanStronglyConnectedComponents(vs, es);
        var big = cs.filter(c->c.length != 1);
        var singletons = cs.filter(c->c.length == 1);
        var selfLoops = singletons.filter(c->es(c[0]).contains(c[0]));
        var bads = big.concat(selfLoops);
        return if (bads.length == 0) {
            var vs2 = cs.map(c->c[0]);
            vs2.reverse();
            Inr(vs2);
        } else Inl(bads);
    }
    
    // Finds strongly connected components of a graph defined by vertices `vs`
    // and edges `es´.
    // Returns a list of components given as a list of vertices.
    static public function tarjanStronglyConnectedComponents(
        vs : Iterable<String>,
        es : String->Iterable<String>
    ) : Array<Array<String>> {
        var index : Int = 0;
        var stack : Array<String> = [];
        var rs : Map<String, TarjanRecord> = new Map();
        var components : Array<Array<String>> = [];
        function getRecord(v : String) : TarjanRecord {
            var r = rs.get(v);
            if (r == null) {
                r = {index: -1, lowlink: -1, stacked: false};
                rs.set(v, r);
            }
            return r;
        }
        function strongConnect(v : String, rv : TarjanRecord) {
            rv.index = index;
            rv.lowlink = index;
            ++index;
            stack.push(v);
            rv.stacked = true;
            for (w in es(v)) {
                var rw = getRecord(w);
                if (rw.index < 0) {
                    strongConnect(w, rw);
                    rv.lowlink = imin(rv.lowlink, rw.lowlink);
                } else if (rw.stacked) {
                    rv.lowlink = imin(rv.lowlink, rw.index);
                }
            }
            if (rv.lowlink == rv.index) {
                var component = [];
                var w = stack.pop();
                var rw = rs.get(w);
                rw.stacked = false;
                component.push(w);
                while (v != w) {
                    w = stack.pop();
                    rw = rs.get(w);
                    rw.stacked = false;
                    component.push(w);
                }
                components.push(component);
            }
        }
        for (v in vs) {
            var r = getRecord(v);
            if (r.index < 0) {
                strongConnect(v, r);
            }
        }
        return components;
    }
}

typedef TarjanRecord = {
    index : Int,
    lowlink : Int,
    stacked : Bool
}