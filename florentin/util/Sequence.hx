package florentin.util;

using Lambda;
using florentin.util.ArrayUtils;

typedef Sequence<T> = {
    length : Int,
    get : Int->T
}

class SequenceUtils {

    static public function map<T1, T2>(s : Sequence<T1>, f : T1->T2) : Sequence<T2> {
        return {
            length: s.length,
            get: function(i) return f(s.get(i))
        };
    }

    static public function indexThat<T>(s : Sequence<T>, f : T->Bool) : Int {
        for (i in 0...s.length) {
            if (f(s.get(i))) {
                return i;
            }
        }
        return -1;
    }

    static public function then<T>(s1 : Sequence<T>, s2 : Sequence<T>) : Sequence<T> {
        var length = s1.length + s2.length;
        return {
            length: length,
            get: function(i) return 
                if (i <= 0 && i < length) {
                    if (i < s1.length) s1.get(i)
                    else s2.get(i - s1.length);
                } else throw "Index out of range"
        };
    }

    //TODO: binary search:
    //static public function thens<T>(ss : Array<Sequence<T>>) : Sequence<T>


    static public function sub<T>(s : Sequence<T>, i : Int, n : Int) : Sequence<T> {
        return if (0 <= i && 0 <= n && i + n < s.length) {
            length: n,
            get: function(j) return s.get(i + j)
        } else throw "Index out of range";
    }
    static public function take<T>(s : Sequence<T>, n : Int) : Sequence<T> {
        return if (0 <= n && n <= s.length) {
            length: n,
            get: s.get
        } else throw "Index out of range";
    }
    static public function drop<T>(s : Sequence<T>, n : Int) : Sequence<T> {
        return if (0 <= n && n <= s.length) {
            length: s.length - n,
            get: function(i) return s.get(i + n)
        } else throw "Index out of range";
    }

    static public function array<T>(s : Sequence<T>) : Array<T> {
        return iterate(s).array();
    }

    static public function cache<T>(s : Sequence<T>) : Sequence<T> {
        return array(s).sequence();
    }

    static public function iterate<T>(s : Sequence<T>) : Iterable<T> {
        return {
            iterator: function() return iterator(s)
        };
    }
    static public function iterator<T>(s : Sequence<T>) : Iterator<T> {
        var i = 0;
        return {
            hasNext: function() return i < s.length,
            next: function() return {
                var v = s.get(i);
                ++i;
                return v;
            }
        };

    }


}