package florentin.util;

using haxe.Http;
using florentin.util.ContinuationUtils3;

class HttpUtils {

    // Gets the url returning the body of the respone.
    static public function httpGet3(url : String) : Cpsf3<String> {
        return function(onDone : String->Void, onError : String->Void) : Void->Void {
            var request = new Http(url);
            var done = false;
            request.async = true;
            request.onData = function(data : String) {
                done = true;
                onDone(data);
            };
            request.onError = function(msg : String) {
                done = true;
                onError('httpGet of $url failed: $msg');
            };
            request.request();
            return function() {
                if (!done) {
                    done = true;
                    request.cancel();
                    onError('httpGet of $url canceled');
                }
            }
        };        
    }

}
