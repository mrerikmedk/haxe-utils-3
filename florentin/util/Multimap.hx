package florentin.util;

using florentin.util.IteratorUtils;
using florentin.util.KeyValue;
using florentin.util.MapUtils;
using florentin.util.Set;

// A multimap is a map where each key can be mapped to multiple values.
class Multimap<TK, TV> {
    public function new(
        map : Map<TK, Set<TV>>, 
        makeSet : ()->Set<TV>
    ) {
        this.map = map;
        this.makeSet = makeSet;
    }

    public function insert(key : TK, value : TV) : Multimap<TK, TV> {
        var map1 = copy();
        map1.insertMutate(key, value);
        return map1;
    }
    public function inserts(pairs : Iterable<KeyValue<TK, TV>>) : Multimap<TK, TV> {
        var map1 = copy();
        for (pair in pairs) {
            map1.insertMutate(pair.key, pair.value);
        }
        return map1;
    }

    public function union(that : Multimap<TK, TV>) : Multimap<TK, TV> {
        return new Multimap(
            map.unionBy(that.map, (set1, set2)->set1.union(set2)),
            makeSet
        );
    }

    public function get(key : TK) : Set<TV> {
        return if (map.exists(key)) map.get(key)
        else makeSet();
    }

    public function keys() : Iterable<TK> {
        return map.keys2();
    }
    public function pairs() : Iterable<KeyValue<TK, Set<TV>>> {
        return keys().mapLazy(key->key.is(map.get(key)));
    }

    public function copy() : Multimap<TK, TV> {
        var map1 = map.copy();
        for (key in map1.keys()) {
            map1.set(key, map1.get(key).copy());
        }
        return new Multimap(map1, makeSet);
    }
    public function insertMutate(key : TK, value : TV) {
        var set = map.get(key);
        if (set == null) {
            set = makeSet();
            map.set(key, set);
        }
        set.insertMutate(value);
    }

    public function toString() : String {
        return MultimapUtils.show(this, key->'$key', value->'$value');
    }

    private var map : Map<TK, Set<TV>>;
    private var makeSet : ()->Set<TV>;
}

class MultimapUtils {
    static public function emptyStringString() : Multimap<String, String> {
        return new Multimap<String, String>(new Map(), ()->SetUtils.emptyString());
    }
    static public function show<TK, TV>(
        map : Multimap<TK, TV>,
        showKey : TK->String,
        showValue : TV->String,
        mapsTo : String = ": ",
        delimiterMap : String = ", ",
        delimiterSet : String = ", ",
        bracesMap : String->String = null,
        bracesSet : String->String = null
    ) : String {
        if (bracesMap == null) {
            bracesMap = s->"{" + s + "}";
        }
        if (bracesSet == null) {
            bracesSet = s->"{" + s + "}";
        }
        return bracesMap(map.pairs().showElements(
            pair->showKey(pair.key) + mapsTo 
          + pair.value.show(showValue, delimiterSet, bracesSet),
            delimiterMap
        ));
    }
}