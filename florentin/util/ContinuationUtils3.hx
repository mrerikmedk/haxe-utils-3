package florentin.util;

using haxe.Timer;

// Cancelable, failable continuation based function.
// onDone->onError->cancel
typedef Cpsf3<T> = (T->Void)->(String->Void)->(Void->Void);

class ContinuationUtils3 {
    static public function ok<T>(v : T) : Cpsf3<T> {
        return function(onDone, onError) return {
            onDone(v);
            function() {};
        };
    }
    static public function fail<T>(error : String) : Cpsf3<T> {
        return function(onDone, onError) return {
            onError(error);
            function() {};
        };
    }

    static public function asyncOk<T>(v : T, ms : Int = 0) : Cpsf3<T> {
        return then(timer(ms), ok(v));
    }
    static public function asyncFail<T>(error : String, ms : Int = 0) : Cpsf3<T> {
        return then(timer(ms), fail(error));
    }

    // Runs f1 and subsequently on success, runs f2. The result of f1 is 
    // ignored.
    static public function then<T1, T2>(f1 : Cpsf3<T1>, f2 : Cpsf3<T2>) : Cpsf3<T2> {
        return mbind(f1, function(_) return f2);
    }

    // Simultaneously starts f1 and f2. Completes when both are completed. If 
    // one fails, the other is canceled and the entire operation fails. The
    // results are ignored.
    static public function and<T1, T2>(f1 : Cpsf3<T1>, f2 : Cpsf3<T2>) : Cpsf3<{}> {
        return andCombine(f1, f2, function(_, _) return {});
    }
    // Starts all functions in fs simultaneously. Completes when all are 
    // completed. Fails if one fails cancelling the rest. Returns a list of
    // the results.
    static public function ands<T>(fs : Array<Cpsf3<T>>) : Cpsf3<Array<T>> {
        return function(onDone : Array<T>->Void, onError : String->Void) {
            var busy : Array<Bool> = fs.map(function(_) return false);
            var values : Array<Null<T>> = fs.map(function(_) return null);
            var nBusy : Int = fs.length;
            var cancels : Array<Void->Void> = [];
            var canceling = false;
            var cancel = function() {
                if (!canceling) {
                    canceling = true;
                    for (i in 0...fs.length) {
                        if (busy[i]) {
                            cancels[i]();
                        }
                    }
                }
            };
            for (i in 0...fs.length) {
                var iCopy = i;
                var onDone1 = function(v) {
                    values[iCopy] = v;
                    busy[iCopy] = false;
                    --nBusy;
                    if (nBusy == 0) {
                        onDone(values);
                    }
                };
                var onError1 = function(message) {
                    busy[iCopy] = false;
                    cancel();
                    onError(message);
                }
                var cancel1 = fs[i](onDone1, onError1);
                cancels.push(cancel1);
                busy[iCopy] = true;
            }
            return cancel;
        };
    }
    // Simultaneously starts f1 and f2. Completes when both are completed. If 
    // one fails, the other is canceled and the entire CPS function fails. The
    // result of the operation is the combination of the results of the 
    // sub-operations by the given function.
    static public function andCombine<T1, T2, T>(f1 : Cpsf3<T1>, f2 : Cpsf3<T2>, combine : T1->T2->T) : Cpsf3<T> {
        return function(onDone : T->Void, onError : String->Void) {
            var cancel1 : Null<Void->Void> = null;
            var cancel2 : Null<Void->Void> = null;
            var failed = false;
            var value1 : Null<T1> = null;
            var value2 : Null<T2> = null;
            cancel1 = f1(
                function(v1) {
                    value1 = v1;
                    cancel1 = null;
                    if (cancel2 == null) {
                        onDone(combine(value1, value2));
                    }
                }, 
                function(msg) {
                    cancel1 = null;
                    if (!failed) {
                        failed = true;
                        if (cancel2 != null) {
                            cancel2();
                        }
                        onError(msg);
                    }
                }
            );
            cancel2 = f2(
                function(v2) {
                    value2 = v2;
                    cancel2 = null;
                    if (cancel1 == null) {
                        onDone(combine(value1, value2));
                    }
                },
                function(msg) {
                    cancel2 = null;
                    if (!failed) {
                        failed = true;
                        if (cancel1 != null) {
                            cancel1();
                        }
                        onError(msg);
                    }
                }
            );
            return function() {
                if (cancel1 != null) {
                    cancel1();
                }
                if (cancel2 != null) {
                    cancel2();
                }
            }

        }
    }

    static public function andRight<T1, T2>(f1 : Cpsf3<T1>, f2 : Cpsf3<T2>) : Cpsf3<T2> {
        return andCombine(f1, f2, function(v1, v2) return v2);
    }

    // Creates a timer that completed af a number of ms. Never fails.
    static public function timer(ms : Int) : Cpsf3<{}> {
        return function(onDone : {}->Void, onError : String->Void) {
            var canceled = false;
            Timer.delay(function() {
                if (!canceled) {
                    onDone({});
                }
            }, ms);
            return function() {
                canceled = true;
                onError("Canceled");
            }
        }
    }

    /*static public function timeout<T>(f : Cpsf<T>, ms : Int) : Cpsf<T> {

    }*/

    // Monad bind over CPS functions. Starts f1, and if f1 completes 
    // successfully, starts f2 with the result of f1. Completes with the 
    // result of f2.
    static public function mbind<T1, T2>(f1 : Cpsf3<T1>, f2 : T1->Cpsf3<T2>) : Cpsf3<T2> {
        return function(onDone : T2->Void, onError : String->Void) {
            var cancel : Null<Void->Void> = null;
            cancel = f1(
                function(v1) {
                    cancel = f2(v1)(function(v2) {
                        cancel = null;
                        onDone(v2);
                    }, function(msg) {
                        cancel = null;
                        onError(msg);
                    });
                }, function(msg) {
                    cancel = null;
                    onError(msg);
                }
            );
            return function() {
                if (cancel != null) {
                    cancel();
                }
            };
        };
    }

    static public function bindError<T>(f1 : Cpsf3<T>, f2 : String->Cpsf3<T>) : Cpsf3<T> {
        return function(onDone, onError) {
            var cancel : Null<Void->Void> = null;
            cancel = f1(
                function(v) {
                    cancel = null;
                    onDone(v);
                },
                function(msg) {
                    cancel = f2(msg)(
                        function(v) {
                            cancel = null;
                            onDone(v);
                        },
                        onError
                    );
                }
            );
            return function() {
                if (cancel != null) {
                    cancel();
                }
            };
        }
    }

    // Applies a function to the result of f.
    static public function apply<T1, T2>(f : Cpsf3<T1>, consumer : T1->T2) : Cpsf3<T2> {
        return function(onDone : T2->Void, onError : String->Void) {
            return f(function(v) onDone(consumer(v)), onError);
        }
    }


    static public function handle<T>(f : Cpsf3<T>, handler : String->T) : Cpsf3<T> {
        return bindError(f, function(msg) return ok(handler(msg)));
    }

    // Consumes the result of an operation. E.g.:
    //   var bibJson : Null<String> = null;
    //   var f = loadBib.consume(function(s){ bibJson = s; });
    static public function consume<T>(f : Cpsf3<T>, consumer : T->Void) : Cpsf3<{}> {
        return apply(f, function(v){
            consumer(v);
            return {};
        });
    }

    static public function await<T>(f : Cpsf3<T>) : Void {
        f(function(_) {}, function(msg) trace('Uncaught error $msg'));
    }

}
