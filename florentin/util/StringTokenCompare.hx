package florentin.util;

using StringTools;
using florentin.util.CharCategories;

class StringTokenCompare {
    // A more natural string compare that understand integer parts. E.g.
    //   "v2" < "v10" according to stringTokenOrder
    //   "v2" > "v10" according to ordinary string compare.
    static public function stringTokenCompare(left : String, right : String) : Int {
        var iLeft : Int = 0;
        var iRight : Int = 0;
        while (iLeft < left.length && iRight < right.length) {
            var digitLeft = left.fastCodeAt(iLeft).isDigit();
            var digitRight = right.fastCodeAt(iRight).isDigit();
            if (digitLeft && digitRight) {
                var iLeftBegin = skipZeros(left, iLeft);
                var iLeftEnd = skipDigits(left, iLeftBegin);
                var iRightBegin = skipZeros(right, iRight);
                var iRightEnd = skipDigits(right, iRightBegin);
                var iLeftLength = iLeftEnd - iLeftBegin;
                var iRightLength = iRightEnd - iRightBegin;
                if (iLeftLength > iRightLength) {
                    return 1;
                } else if (iLeftLength < iRightLength) {
                    return -1;
                } else {
                    for (i in 0...iLeftLength) {
                        var cLeft = left.fastCodeAt(iLeftBegin + i);
                        var cRight = right.fastCodeAt(iRightBegin + i);
                        if (cLeft > cRight) {
                            return 1;
                        } else if (cLeft < cRight) {
                            return -1;
                        }
                    }
                }
                iLeft = iLeftEnd;
                iRight = iRightEnd;
            } else if (!digitLeft && digitRight) {
                return 1;
            } else if (digitLeft && !digitRight) {
                return -1;
            } else {
                var cLeft = left.fastCodeAt(iLeft);
                var cRight = right.fastCodeAt(iRight);
                if (cLeft > cRight) {
                    return 1;
                } else if (cLeft < cRight) {
                    return -1;
                }
                ++iLeft;
                ++iRight;
            }
        }
        if (iLeft < left.length && iRight == right.length) {
            return 1;
        } else if (iLeft == left.length && iRight < right.length) {
            return -1;
        } else {
            return 0;
        }
    }
    static private function skipDigits(s : String, i : Int) : Int {
        while (i < s.length && s.fastCodeAt(i).isDigit()) {
            ++i;
        }
        return i;
    }
    static private function skipZeros(s : String, i : Int) : Int {
        while (i < s.length && s.fastCodeAt(i) == "0".code) {
            ++i;
        }
        return i;
    }
    
}
