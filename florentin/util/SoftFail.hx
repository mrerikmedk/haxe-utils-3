package florentin.util;

// A value with a flags indicating that a problem was detected in deriving the 
// value. 
typedef SoftFail<T> = {
    value : T,
    failed : Bool
};

class SoftFailUtils {
    static public function softOk<T>(value : T) : SoftFail<T> {
        return {value: value, failed: false};
    }
    static public function softFailed<T>(value : T) : SoftFail<T> {
        return {value: value, failed: true};
    }

    static public function bind<T1, T2>(v : SoftFail<T1>, f : T1->SoftFail<T2>) : SoftFail<T2> {
        var v2 = f(v.value);
        return {value: v2.value, failed: v.failed || v2.failed};
    }
}
