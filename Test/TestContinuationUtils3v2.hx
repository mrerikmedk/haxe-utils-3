package;

import haxe.PosInfos;
import haxe.Timer;
import haxe.unit.TestCase;
using florentin.util.ContinuationUtils3;
using florentin.util.UnitTestUtils;


// TestCase is not super happy about testing async stuff. You may get nasty 
// looking assertions instead of beautiful error messages test failures.
// Consider different unit test library.
class TestContinuationUtils3v2 extends TestCase {
    public function testSyncOk() {
        var p = makeProcess(syncOk("hi"));
        runSync()(p);
        Timer.delay(function() {
            assertOk("hi")(p);
        }, 100);
    }
    public function testAsyncOk() {
        var p = makeProcess(asyncOk2("hi", 20));
        runAsync()(p);
        Timer.delay(function() {
            assertOk("hi")(p);
        }, 100);
    }
    public function testSyncFail() {
        var p = makeProcess(syncFail("err"));
        runSync()(p);
        Timer.delay(function() {
            assertFail("err")(p);
        }, 100);
    }
    public function testAsyncFail() {
        var p = makeProcess(asyncFail("err", 20));
        runAsync()(p);
        Timer.delay(function() {
            assertFail("err")(p);
        }, 100);
    }
    public function testCancelOk() {
        var p = makeProcess(asyncOk2("hi", 50));
        runCancel(20)(p);
        Timer.delay(function() {
            assertCancel()(p);
        }, 100);
    }
    public function testCancelFail() {
        var p = makeProcess(asyncFail("hi", 50));
        runCancel(20)(p);
        Timer.delay(function() {
            assertCancel()(p);
        }, 100);
    }

    public function testThen() {
        var then = ContinuationUtils3.then;
        binaryCase(syncOk("a"), syncOk("b"), then, runSync(), 
            assertOk("a"), assertOk("b"), assertOk("b"), 20
        );
        binaryCase(syncOk("a"), asyncOk2("b", 10), then, runAsync(), 
            assertOk("a"), assertOk("b"), assertOk("b"), 50
        );
        binaryCase(asyncOk2("a", 10), syncOk("b"), then, runAsync(), 
            assertOk("a"), assertOk("b"), assertOk("b"), 50
        );
        binaryCase(asyncOk2("a", 10), asyncOk2("b", 10), then, runAsync(), 
            assertOk("a"), assertOk("b"), assertOk("b"), 50
        );

        binaryCase(syncFail("a"), syncOk("b"), then, runSync(),
            assertFail("a"), assertUnstarted(), assertFail("a"), 20
        );
        binaryCase(syncFail("a"), asyncOk2("b", 10), then, runSync(),
            assertFail("a"), assertUnstarted(), assertFail("a"), 20
        );
        binaryCase(syncOk("a"), syncFail("b"), then, runSync(),
            assertOk("a"), assertFail("b"), assertFail("b"), 20
        );
        binaryCase(asyncOk2("a", 10), syncFail("b"), then, runAsync(),
            assertOk("a"), assertFail("b"), assertFail("b"), 50
        );

        binaryCase(asyncFail("a", 10), syncOk("b"), then, runAsync(),
            assertFail("a"), assertUnstarted(), assertFail("a"), 50
        );
        binaryCase(asyncFail("a", 10), asyncOk2("b", 10), then, runAsync(),
            assertFail("a"), assertUnstarted(), assertFail("a"), 50
        );
        binaryCase(syncOk("a"), asyncFail("b", 10), then, runAsync(),
            assertOk("a"), assertFail("b"), assertFail("b"), 50
        );
        binaryCase(asyncOk2("a", 10), asyncFail("b", 10), then, runAsync(),
            assertOk("a"), assertFail("b"), assertFail("b"), 50
        );

        binaryCase(syncFail("a"), syncFail("b"), then, runSync(),
            assertFail("a"), assertUnstarted(), assertFail("a"), 20
        );
        binaryCase(syncFail("a"), asyncFail("b", 10), then, runSync(),
            assertFail("a"), assertUnstarted(), assertFail("a"), 50
        );
        binaryCase(asyncFail("a", 10), syncFail("b"), then, runAsync(),
            assertFail("a"), assertUnstarted(), assertFail("a"), 50
        );
        binaryCase(asyncFail("a", 10), asyncFail("b", 10), then, runAsync(),
            assertFail("a"), assertUnstarted(), assertFail("a"), 50
        );

        //TODO: cancel
    }

    static private function binaryCase<T1, T2, T3>(
        f1 : Cpsf3<T1>, f2 : Cpsf3<T2>, f : Cpsf3<T1>->Cpsf3<T2>->Cpsf3<T3>,
        run : Process<T3>->Void,
        f1Assert : Process<T1>->Void,
        f2Assert : Process<T2>->Void,
        fAssert : Process<T3>->Void,
        ms : Int
    ) : Void {
        var p1 = makeProcess(f1);
        var p2 = makeProcess(f2);
        var p = makeProcess(f(p1.p, p2.p));
        run(p);
        Timer.delay(function() {
            f1Assert(p1);
            f2Assert(p2);
            fAssert(p);
        }, ms);
    }


    static private function syncOk<T>(v : T) : Cpsf3<T> {
        return function(onDone, onFail) return {
            onDone(v);
            function() {};
        };
    }
    static private function asyncOk2<T>(v : T, ms : Int) : Cpsf3<T> {
        return function(onDone, onFail) return {
            Timer.delay(function() onDone(v), ms);
            function() {};
        };
    }
    static private function syncFail<T>(msg : String) : Cpsf3<T> {
        return function(onDone, onFail) return {
            onFail(msg);
            function() {};
        };
    }
    static private function asyncFail<T>(msg : String, ms : Int) : Cpsf3<T> {
        return function(onDone, onFail) return {
            Timer.delay(function() onFail(msg), ms);
            function() {};
        };
    }


    private function runSync<T>(?pos : PosInfos) : Process<T>->Void {
        currentTest.done = true;
        return function(p) {
            var returned = false;
            p.p(
                function(_) {
                    if (returned) {
                        this.failWith('Process completed asynchronous. Expected synchronous.', pos);
                    }
                },
                function(_) {
                    if (returned) {
                        this.failWith('Process failed asynchronous. Expected synchronous.', pos);
                    }
                }
            );
            returned = true;
        }
    }
    private function runAsync<T>(?pos : PosInfos) : Process<T>->Void {
        currentTest.done = true;
        return function(p) {
            var returned = false;
            p.p(
                function(_) {
                    if (!returned) {
                        this.failWith('Process completed synchronous. Expected asynchronous.', pos);
                    }
                },
                function(_) {
                    if (!returned) {
                        this.failWith('Process failed synchronous. Expected asynchronous.', pos);
                    }
                }
            );
            returned = true;
        }
    }
    private function runCancel<T>(ms : Int, ?pos : PosInfos) : Process<T>->Void {
        currentTest.done = true;
        return function(p) {
            var returned = false;
            var cancel = p.p(
                function(_) {
                    if (!returned) {
                        this.failWith('Process completed synchronous. Expected asynchronous.', pos);
                    }
                },
                function(_) {
                    if (!returned) {
                        this.failWith('Process failed synchronous. Expected asynchronous.', pos);
                    }
                }
            );
            returned = true;
            Timer.delay(cancel, ms);
        }
    }

    private function assertUnstarted<T>(?pos : PosInfos) : Process<T>->Void {
        currentTest.done = true;
        return function(p) {
            if (p.starts > 0) {
                this.failWith('Expected process to be unstarted (${p.starts}).', pos);
            }
        }
    }
    private function assertOk<T>(v : T, ?pos : PosInfos) : Process<T>->Void {
        currentTest.done = true;
        return function(p) {
            if (p.dones == 0) {
                this.failWith('Expected process to complete.', pos);
            }
            if (p.dones > 1) {
                this.failWith('Process completed multiple times (${p.dones}).', pos);
            }
            if (p.fails > 0) {
                this.failWith('Process expected to complete failed (${p.fails}).', pos);
            }
            if (p.cancels > 0) {
                this.failWith('Did not expected process to be canceled (${p.cancels}).', pos);
            }
            if (p.starts != 1) {
                this.failWith('Expected process to be started once (${p.starts}).', pos);
            }
            assertEquals(v, p.value, pos);
        }
    }
    private function assertFail<T>(msg : String, ?pos : PosInfos) : Process<T>->Void {
        currentTest.done = true;
        return function(p) {
            if (p.fails == 0) {
                this.failWith('Expected process to fail.', pos);
            }
            if (p.fails > 1) {
                this.failWith('Process failed multiple times (${p.fails}).', pos);
            }
            if (p.dones > 0) {
                this.failWith('Process expected to fail completed (${p.dones}).', pos);
            }
            if (p.cancels > 0) {
                this.failWith('Did not expected process to be canceled (${p.cancels}).', pos);
            }
            if (p.starts != 1) {
                this.failWith('Expected process to be started once (${p.starts}).', pos);
            }
            assertEquals(msg, p.error, pos);
        }
    }
    private function assertCancel<T>(?pos : PosInfos) : Process<T>->Void {
        currentTest.done = true;
        return function(p) {
            if (p.cancels == 0) {
                this.failWith('Expected process to be canceled.', pos);
            }
            if (p.cancels > 1) {
                this.failWith('Process canceled multiple times (${p.cancels}).', pos);
            }
            if (!(p.dones == 1 && p.fails == 0 || p.dones == 0 && p.fails == 1)) {
                this.failWith('Expected process to complete or fail once (${p.dones}, ${p.fails}).', pos);
            }
            if (p.starts != 1) {
                this.failWith('Expected process to be started once (${p.starts}).', pos);
            }
        }
    }

    static private function makeProcess<T>(f : Cpsf3<T>) : Process<T> {
        var p = {
            p: null,
            value: null,
            error: null,
            starts: 0,
            dones: 0,
            fails: 0,
            cancels: 0
        };
        p.p = function(onDone, onError) {
            ++p.starts;
            var cancel = f(
                function(v) {
                    ++p.dones;
                    p.value = v;
                    onDone(v);
                },
                function(msg) {
                    ++p.fails;
                    p.error = msg;
                    onError(msg);
                }
            );
            return function() {
                ++p.cancels;
                cancel();
            }
        };
        return p;
    }
}

typedef Process<T> = {
    p : Cpsf3<T>,
    value : Null<T>,
    error : Null<String>,
    starts : Int,
    dones : Int,
    fails : Int,
    cancels : Int
}
