package;

using haxe.unit.TestRunner;

class Main {
    static function main() {
        var runner = new TestRunner();
        
        runner.add(new TestArrayUtils());
        runner.add(new TestContinuationUtils3());
        runner.add(new TestContinuationUtils3v2());
        runner.add(new TestFailable());
        runner.add(new TestHxEscape());
        runner.add(new TestIteratorUtils());
        runner.add(new TestMap2Utils());
        runner.add(new TestMapUtils());
        runner.add(new TestMathUtils());
        runner.add(new TestMultimap());
        runner.add(new TestProcess());
        runner.add(new TestSet());
        runner.add(new TestStringUtils());

        runner.run();
    }
}