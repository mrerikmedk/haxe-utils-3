package;

using Lambda;
using haxe.PosInfos;
using haxe.Timer;
using haxe.unit.TestCase;
using florentin.util.ContinuationUtils3;
using florentin.util.UnitTestUtils;

typedef TestFlags = {
    started : Bool, 
    done : Bool, 
    failed : Bool, 
    canceled : Bool
};

// TestCase is not super happy about testing async stuff. You may get nasty 
// looking assertions instead of beautiful error messages test failures.
// Consider different unit test library.
class TestContinuationUtils3 extends TestCase {
    public function testThen() {
        {   // two syncs
            var flags1 = makeFlags();
            var flags2 = makeFlags();
            assertOkEq(2, 
                syncOk("1", flags1).then(syncOk(2, flags2)), 
                function(){
                    assertOkFlags(flags1);
                    assertOkFlags(flags2);
                }
            );
        }
        {   // two asyncs
            var flags1 = makeFlags();
            var flags2 = makeFlags();
            assertOkEq(2, 
                asyncOk2("1", 10, flags1).then(asyncOk2(2, 20, flags2)), 
                function(){
                    assertOkFlags(flags1);
                    assertOkFlags(flags2);
                }
            );
        }
        {   // sync, async
            var flags1 = makeFlags();
            var flags2 = makeFlags();
            assertOkEq(2, 
                syncOk("1", flags1).then(asyncOk2(2, 10, flags2)), 
                function(){
                    assertOkFlags(flags1);
                    assertOkFlags(flags2);
                }
            );
        }
        {   // async, sync
            var flags1 = makeFlags();
            var flags2 = makeFlags();
            assertOkEq(2, 
                asyncOk2("1", 10, flags1).then(syncOk(2, flags2)), 
                function(){
                    assertOkFlags(flags1);
                    assertOkFlags(flags2);
                }
            );
        }

        {   // ok, fail
            var flags1 = makeFlags();
            var flags2 = makeFlags();
            assertFailWith("fail2",
                asyncOk2("1", 20, flags1).then(asyncFail("fail2", 10, flags2)), 
                function(){
                    assertOkFlags(flags1);
                    assertFailFlags(flags2);
                }
            );
        }
        {   // fail, ok
            var flags1 = makeFlags();
            var flags2 = makeFlags();
            assertFailWith("fail1",
                asyncFail("fail1", 20, flags1).then(asyncOk2(2, 10, flags2)), 
                function(){
                    assertFailFlags(flags1);
                    assertUnstartedFlags(flags2);
                }
            );
        }
        {   // fail sync, ok
            var flags1 = makeFlags();
            var flags2 = makeFlags();
            assertFailWith("fail1",
                syncFail("fail1", flags1).then(asyncOk2(2, 10, flags2)), 
                function(){
                    assertFailFlags(flags1);
                    assertUnstartedFlags(flags2);
                }
            );
        }
        {   // fail, fail
            var flags1 = makeFlags();
            var flags2 = makeFlags();
            assertFailWith("fail1",
                asyncFail("fail1", 20, flags1).then(asyncFail("fail2", 10, flags2)), 
                function(){
                    assertFailFlags(flags1);
                    assertUnstartedFlags(flags2);
                }
            );
        }
    }


    static public function makeFlags() : TestFlags {
        return {started: false, done: false, failed: false, canceled: false};
    }

    static public function syncOk<T>(value : T, ?flags : TestFlags) : Cpsf3<T> {
        return function(onDone : T->Void, onError : String->Void) {
            if (flags != null) {
                flags.started = true;
                flags.done = true;
            }
            onDone(value);
            return function() {};
        }
    }

    static public function syncFail<T>(message : String, ?flags : TestFlags) : Cpsf3<T> {
        return function(onDone : T->Void, onError : String->Void) {
            if (flags != null) {
                flags.started = true;
                flags.done = true;
            }
            onError(message);
            return function() {};
        }
    }

    static public function asyncOk2<T>(value : T, ms : Int, ?flags : TestFlags) : Cpsf3<T> {
        return function(onDone : T->Void, onError : String->Void) {
            if (flags != null) {
                flags.started = true;
            }
            var canceled = false;
            Timer.delay(function() {
                if (!canceled) {
                    if (flags != null) {
                        flags.done = true;
                    }
                    onDone(value);
                }
            }, ms);
            return function() { 
                if (flags != null) {
                    flags.canceled = true;
                }
                canceled = true; 
            };
        }
    }

    static public function asyncFail<T>(message : String, ms : Int, ?flags : TestFlags) : Cpsf3<T> {
        return function(onDone : T->Void, onError : String->Void) {
            if (flags != null) {
                flags.started = true;
            }
            var canceled = false;
            Timer.delay(function() {
                if (!canceled) {
                    if (flags != null) {
                        flags.done = true;
                    }
                    onError(message);
                }
            }, ms);
            return function() { 
                if (flags != null) {
                    flags.canceled = true;
                }
                canceled = true; 
            };
        }
    }

    public function assertOkEq<T>(
        expected : T, 
        f : Cpsf3<T>, 
        onDone : Void->Void,
        ?pos : PosInfos
    ) : Void {
        assertOkEqBy(expected, function(x, y) return x == y, f, onDone, pos);
    }
    public function assertOkEqBy<T1, T2>(
        expected : T1, eq : T1->T2->Bool, 
        f : Cpsf3<T2>, 
        onDone : Void->Void,
        ?pos : PosInfos
    ) : Void {
        currentTest.done = true;
        f(
            function(actual) {
                if (!eq(expected, actual)) {
                    this.failWith('Continuation succeeded with the wrong value $actual. Expected $expected.', pos);
                }
                onDone();
            },
            function(msg) {
                this.failWith('Continuation failed: $msg', pos);
                onDone();
            }
        );
    }

    public function assertFailWith<T>(
        expectedMsg : String,
        f : Cpsf3<T>, 
        onDone : Void->Void,
        ?pos : PosInfos
    ) {
        currentTest.done = true;
        f(
            function(actual) {
                this.failWith('Continuation succeeded but was expected to fail.', pos);
            },
            function(actualMsg) if (expectedMsg != actualMsg) {
                this.failWith('Expected continuation to fail with "$expectedMsg" instead of "$actualMsg".', pos);
            }
        );
    }

    public function assertOkFlags(flags : TestFlags, ?pos : PosInfos) : Void {
        if (!flags.started) {
            this.failWith('Continuation not marked as started.', pos);
        }
        if (!flags.done) {
            this.failWith('Continuation not marked as done.', pos);
        }
        if (flags.failed) {
            this.failWith('Continuation was unexpectedly marked as failed.', pos);
        }
        if (flags.canceled) {
            this.failWith('Continuation was unexpectedly marked as canceled.', pos);
        }
    }

    public function assertFailFlags(flags : TestFlags, ?pos : PosInfos) : Void {
        if (!flags.started) {
            this.failWith('Continuation not marked as started.', pos);
        }
        if (flags.done) {
            this.failWith('Continuation was unexpectedly marked as done.', pos);
        }
        if (!flags.failed) {
            this.failWith('Continuation not marked as failed.', pos);
        }
        if (flags.canceled) {
            this.failWith('Continuation was unexpectedly marked as canceled.', pos);
        }
    }

    public function assertUnstartedFlags(flags : TestFlags, ?pos : PosInfos) : Void {
        if (flags.started) {
            this.failWith('Continuation was unexpectedly marked as started.', pos);
        }
        if (flags.done) {
            this.failWith('Continuation was unexpectedly marked as done.', pos);
        }
        if (flags.failed) {
            this.failWith('Continuation was unexpectedly marked as failed.', pos);
        }
        if (flags.canceled) {
            this.failWith('Continuation was unexpectedly marked as canceled.', pos);
        }
    }
}