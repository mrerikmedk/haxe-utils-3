package;

using Lambda;
using haxe.PosInfos;
using haxe.unit.TestCase;
using florentin.util.IteratorUtils;
using florentin.util.MapUtils;
using florentin.util.UnitTestUtils;
using TestIteratorUtils;
using TestMapUtils;

class TestMapUtils extends TestCase {

    public function testShowMapElements() {
        {
            var s = [1=>true, 2=>false].showMapElements(
                function(i) return '$i',
                function(b) return if (b) "true" else "false",
                "->", " and "
            );
            assertTrue("1->true and 2->false" == s 
                    || "2->false and 1->true" == s);
        }
    }

    public function testMapValuesString() {
        {
            var s = ["x"=>7, "y"=>117]
                .mapValuesString(function(v) return v + 1)
                .showMap();
            assertTrue("{x:8, y:118}" == s
                    || "{y:118, x:8}" == s);
        }
    }

    public function testPairs() {
        {
            var s = new Map<String, String>()
                .pairs()
                .showPairs();
            assertTrue("[]" == s);
        }
        {
            var s = [7=>"x", 8=>"hello"]
                .pairs()
                .showPairs();
            assertTrue("[{key:7, value:x}, {key:8, value:hello}]" == s
                    || "[{key:8, value:hello}, {key:7, value:x}]" == s);
        }
    }

    static public function showMap<TK, TV>(map : Map<TK, TV>) : String {
        return "{" + map.showMapElements(
            function(k) return '$k',
            function(v) return '$v',
            ":", ", "
        ) + "}";
    }
    static public function showPairs<TK, TV>(iter : Iterable<{key : TK, value : TV}>) : String {
        return "[" + iter.showElements(function(pair) return 
            '{key:${pair.key}, value:${pair.value}}', ", "
        ) + "]";
    }

}
