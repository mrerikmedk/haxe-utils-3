package;

using Lambda;
using haxe.PosInfos;
using haxe.unit.TestCase;
using florentin.util.IteratorUtils;
using florentin.util.UnitTestUtils;
using TestIteratorUtils;

class TestIteratorUtils extends TestCase {
    public function testEmpty() {
        this.assertTrue([].empty());
        this.assertFalse([7].empty());
        this.assertFalse(infinite().empty());
    }
    
    public function testAtLeast() {
        assertTrue([1, 2].atLeast(0));
        assertTrue([1, 2].atLeast(1));
        assertTrue([1, 2].atLeast(2));
        assertFalse([1, 2].atLeast(3));
        assertTrue([].atLeast(0));
        assertFalse([].atLeast(1));
        assertTrue(infinite().atLeast(117));
    }

    public function testTake() {
        this.assertEqInts([1, 2, 3], [1, 2, 3, 4, 5].take(3));
        this.assertEqInts([1, 2], [1, 2].take(3));
        this.assertEqInts([], [].take(3));
        this.assertEqInts([0, 1, 2], infinite().take(3));
    }

    public function testFilterLazy() {
        function odd(i : Int) : Bool {
            return i % 2 == 1;
        }
        this.assertEqInts([], [].filterLazy(odd));
        this.assertEqInts([1, 3 ,5], [1, 2, 3, 4, 5, 6].filterLazy(odd));
        this.assertEqInts([1, 3 ,5], [1, 3, 5].filterLazy(odd));
        this.assertEqInts([], [2, 4, 6].filterLazy(odd));
        this.assertEqInts([1, 3, 5], infinite().filterLazy(odd).take(3));

    }

    public function testShowElements() {
        assertEquals("", [].showElements(function(i) return '$i', ":"));
        assertEquals("1", [1].showElements(function(i) return '$i', ":"));
        assertEquals("1:2", [1, 2].showElements(function(i) return '$i', ":"));
    }

    public function testEqBy() {
        assertTrue([1, 2, 3].eqBy(["1", "2", "3"], function(i, s) return '$i' == s));
        assertFalse([1, 2, 33].eqBy(["1", "2", "3"], function(i, s) return '$i' == s));
        assertFalse([1, 2].eqBy(["1", "2", "3"], function(i, s) return '$i' == s));
        assertFalse([1, 2, 3].eqBy(["1", "2", "3", "4"], function(i, s) return '$i' == s));
    }

    public function testMapLazy() {
        var f = function(s) return s.length;
        var g = function(i) return 2 * i;
        this.assertEqInts([], [].mapLazy(f));
        this.assertEqInts([5, 6], ["Hello", "world!"].mapLazy(f));
        this.assertEqInts([0, 2, 4, 6], infinite().mapLazy(g).take(4));
    }

    public function testUniqueBy() {
        var key = function(i) return '${i % 10}';
        this.assertEqInts([], [].uniqueByString(key));
        this.assertEqInts([5, 7], [5, 7].uniqueByString(key));
        this.assertEqInts([5, 7], [5, 7, 15].uniqueByString(key));
        this.assertEqInts(
            [4, 9, 16, 25, 81, 100], 
            infinite()
                .drop(2)
                .mapLazy(function(i) return i * i)
                .uniqueByString(key)
                .take(6)
            );
    }

    static public function infinite() : Iterable<Int> {
        return {
            iterator: function() {
                var i = 0;
                return {
                    hasNext: function() return true,
                    next: function() {
                        var value = i;
                        ++i;
                        return value;
                    }
                };
            }
        };
    }

    static public function assertEqInts(test : TestCase, 
        expected : Iterable<Int>, actual : Iterable<Int>,
        ?pos : PosInfos
    ) : Void {
        var show = function(is : Iterable<Int>) return 
            "[" + is.showElements(function(i) return '$i', ", ") + "]";
        test.assertEqByShow(expected, actual, 
            function(as, bs) return as.eqBy(bs, function(a, b) return a == b), 
            show, show, 
            pos
        );
    }
}
