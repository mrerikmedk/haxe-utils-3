package;

using Lambda;
using haxe.unit.TestCase;
using florentin.util.HxEscape;

class TestHxEscape extends TestCase {
    public function testHxEscape() {
        assertEquals("\"hi\"", "hi".hxEscape());
        assertEquals("\"\"", "".hxEscape());
        assertEquals("\"\\\"\\n\\'\\r\\\\\\t\"", "\"\n'\r\\\t".hxEscape());
    }
}