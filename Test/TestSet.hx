package;

using Lambda;
using haxe.unit.TestCase;
using florentin.util.Set;

class TestSet extends TestCase {
    public function testIsEmpty() {
        assertTrue(new Set<String>(new Map()).isEmpty());
        assertFalse(new Set<String>(["hello world"=>"hello world"]).isEmpty());
    }
    public function testContains() {
        assertTrue(new Set<String>(["a"=>"a", "b"=>"b"]).contains("a"));
        assertFalse(new Set<String>(new Map()).contains("nothing"));
    }
    public function testInsert() {
        var s0 = new Set<String>(["a"=>"a"]);
        var s1 = s0.insert("b");
        assertTrue(s0.contains("a"));
        assertFalse(s0.contains("b"));
        assertTrue(s1.contains("a"));
        assertTrue(s1.contains("b"));
    }
    public function testInserts() {
        var s0 = new Set<String>(["a"=>"a"]);
        var s1 = s0.inserts(["b", "c"]);
        assertTrue(s0.contains("a"));
        assertFalse(s0.contains("b"));
        assertFalse(s0.contains("c"));
        assertTrue(s1.contains("a"));
        assertTrue(s1.contains("b"));
        assertTrue(s1.contains("c"));
    }
    public function testRemove() {
        var s0 = new Set<String>(["a"=>"a", "b"=>"b"]);
        var s1 = s0.remove("a").remove("c");
        assertTrue(s0.contains("a"));
        assertTrue(s0.contains("b"));
        assertFalse(s1.contains("a"));
        assertTrue(s1.contains("b"));
    }
    public function testRemoves() {
        var s0 = new Set<String>(["a"=>"a", "b"=>"b", "c"=>"c"]);
        var s1 = s0.removes(["a", "c", "e"]);
        assertTrue(s0.contains("a"));
        assertTrue(s0.contains("b"));
        assertTrue(s0.contains("c"));
        assertFalse(s1.contains("a"));
        assertTrue(s1.contains("b"));
        assertFalse(s1.contains("c"));
    }
    public function testUnion() {
        var s1 = ["1", "2"].setOfStrings();
        var s2 = ["2", "3"].setOfStrings();
        var s3 = s1.union(s2);
        assertTrue(s1.contains("1"));
        assertTrue(s1.contains("2"));
        assertFalse(s1.contains("3"));
        assertFalse(s2.contains("1"));
        assertTrue(s2.contains("2"));
        assertTrue(s2.contains("3"));
        assertTrue(s3.contains("1"));
        assertTrue(s3.contains("2"));
        assertTrue(s3.contains("3"));
    }
}