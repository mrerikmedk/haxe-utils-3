package;

using Lambda;
using haxe.unit.TestCase;
using florentin.util.StringUtils;

class TestStringUtils extends TestCase {
    public function testRepeat() {
        assertEquals("", "xy".repeat(0));
        assertEquals("xy", "xy".repeat(1));
        assertEquals("xyxyxy", "xy".repeat(3));
    }

    public function testReverse() {
        assertEquals("", "".reverse());
        assertEquals("x", "x".reverse());
        assertEquals("zyx", "xyz".reverse());
    }

    public function testLimit() {
        assertEquals("foo", "foo".limit(5, "<->"));
        assertEquals("", "".limit(5, "<->"));
        assertEquals("He<->", "Hello world!".limit(5, "<->"));
        assertEquals("<->", "Hello world!".limit(3, "<->"));
        assertEquals("<-", "Hello world!".limit(2, "<->"));
        assertEquals("Hello", "Hello world!".limit(5, ""));
        assertEquals("Hello...", "Hello world!".limit(8));
    }

    public function testLimitBack() {
        assertEquals("", "".limitBack(5, "<->"));
        assertEquals("foo", "foo".limitBack(5, "<->"));
        assertEquals("<->d!", "Hello world!".limitBack(5, "<->"));
        assertEquals("<->", "Hello world!".limitBack(3, "<->"));
        assertEquals("->", "Hello world!".limitBack(2, "<->"));
        assertEquals("orld!", "Hello world!".limitBack(5, ""));
        assertEquals("...orld!", "Hello world!".limitBack(8));
    }

    public function testStartsWith() {
        assertTrue("Hello world!".startsWith("Hello"));
        assertTrue("Hello world!".startsWith(""));
        assertTrue("".startsWith(""));
        assertFalse("Hello world!".startsWith("bonjour"));
        assertFalse("".startsWith(" "));
    }

    public function testParenthesize() {
        assertEquals("(hi)", "hi".parenthesize(true));
        assertEquals("hi", "hi".parenthesize(false));
        assertEquals("()", "".parenthesize(true));
        assertEquals("", "".parenthesize(false));
    }

    //todo: testSplitLines()

    public function testGetLine() {
        assertEquals("Hello world", "Hello world".getLine(0));
        assertEquals("ello world", "Hello world".getLine(1));
        assertEquals("Hello", "Hello\nworld".getLine());
        assertEquals("Hello", "Hello\rworld".getLine());
        assertEquals("Hello", "Hello\r\nworld".getLine());
        assertEquals("Hello", "Hello\n\rworld".getLine());
        assertEquals("", "".getLine());
    }

    public function testIndexOfEol() {
        assertEquals(5, "hello\nworld".indexOfEol());
        assertEquals(5, "hello\rworld".indexOfEol());
        assertEquals(5, "hello\r\nworld".indexOfEol());
        assertEquals(5, "hello\n\rworld".indexOfEol());
        assertEquals(11, "hello world".indexOfEol());
        assertEquals(0, "".indexOfEol());
        assertEquals(5, "hello\nworld".indexOfEol(4));
        assertEquals(5, "hello\nworld".indexOfEol(5));
        assertEquals(11, "hello\nworld\n!".indexOfEol(6));
        assertEquals(12, "hello\nworld!".indexOfEol(6));
    }

    public function testEolSize() {
        assertEquals(1, "[\n]".eolSize(1));
        assertEquals(1, "[\r]".eolSize(1));
        assertEquals(2, "[\r\n]".eolSize(1));
        assertEquals(2, "[\n\r]".eolSize(1));
        assertEquals(0, "foo".eolSize(3));
        assertEquals(-1, "[.]".eolSize(1));
    }

    public function testCodes() {
        {
            var cs = "hi".codes().array();
            assertEquals(2, cs.length);
            assertEquals("h".code, cs[0]);
            assertEquals("i".code, cs[1]);
        }
        {
            var cs = "".codes().array();
            assertEquals(0, cs.length);
        }
    }

}