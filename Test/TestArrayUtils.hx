package;

using florentin.util.ArrayUtils;
import haxe.unit.TestCase;

class TestArrayUtils extends TestCase {
    public function testIsPermutationSlow() {
        {   // Empty arrays
            assertTrue(ArrayUtils.isPermutationSlow([], []));
            assertFalse(ArrayUtils.isPermutationSlow([], [3, 1, 4]));
            assertFalse(ArrayUtils.isPermutationSlow([2, 7, 1], []));
        }
        {
            // Single element permuted equivalent arrays
            assertTrue(ArrayUtils.isPermutationSlow([7], [7]));
        }
        {   // Single element permuted non-equivalent arrays.
            assertFalse(ArrayUtils.isPermutationSlow([7], [117]));
        }
        {   // Zero and one element arrays not permutations.
            assertFalse(ArrayUtils.isPermutationSlow([], [7]));
            assertFalse(ArrayUtils.isPermutationSlow([7], []));
        }
        {   // Swapped two elements.
            assertTrue(ArrayUtils.isPermutationSlow([7, 117], [117, 7]));
        }
        {   // Repeated elements in permutations
            assertTrue(ArrayUtils.isPermutationSlow([1, 2, 1], [2, 1, 1]));
            assertTrue(ArrayUtils.isPermutationSlow([1, 1, 2], [1, 2, 1]));
        }
        {   // Count of duplicates is important.
            assertFalse(ArrayUtils.isPermutationSlow([1], [1, 1]));
            assertFalse(ArrayUtils.isPermutationSlow([1, 1], [1]));
            assertFalse(ArrayUtils.isPermutationSlow([1], [1, 2, 1]));
            assertFalse(ArrayUtils.isPermutationSlow([1, 2, 1], [1]));
            assertFalse(ArrayUtils.isPermutationSlow([7, 7], [7, 7, 7]));
            assertFalse(ArrayUtils.isPermutationSlow([7, 7, 7], [7, 7]));
        }
    }

    public function testBinaryPartition() {
        assertEquals(0, [].binaryPartition(a->a >= 0));
        
        assertEquals(0, [1].binaryPartition(a->a >= 0));
        assertEquals(0, [1].binaryPartition(a->a >= 1));
        assertEquals(1, [1].binaryPartition(a->a >= 2));

        assertEquals(0, [1, 3].binaryPartition(a->a >= 0));
        assertEquals(0, [1, 3].binaryPartition(a->a >= 1));
        assertEquals(1, [1, 3].binaryPartition(a->a >= 2));
        assertEquals(1, [1, 3].binaryPartition(a->a >= 3));
        assertEquals(2, [1, 3].binaryPartition(a->a >= 4));

        assertEquals(0, [1, 3, 5].binaryPartition(a->a >= 0));
        assertEquals(0, [1, 3, 5].binaryPartition(a->a >= 1));
        assertEquals(1, [1, 3, 5].binaryPartition(a->a >= 2));
        assertEquals(1, [1, 3, 5].binaryPartition(a->a >= 3));
        assertEquals(2, [1, 3, 5].binaryPartition(a->a >= 4));
        assertEquals(2, [1, 3, 5].binaryPartition(a->a >= 5));
        assertEquals(3, [1, 3, 5].binaryPartition(a->a >= 6));
    }
}