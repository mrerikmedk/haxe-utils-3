package;

using Lambda;
using haxe.unit.TestCase;
using florentin.util.MapUtils;
using florentin.util.Multimap;
using florentin.util.Set;

class TestMultimap extends TestCase {
    public function testInsert() {
        var m0 = MultimapUtils.emptyStringString();
        var m1 = m0.insert("foo", "1").insert("bar", "2").insert("foo", "3");
        assertTrue(m1.get("foo").eq(["1", "3"].setOfStrings()));
        assertTrue(m1.get("bar").eq(["2"].setOfStrings()));
    }
}