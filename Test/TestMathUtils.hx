package;

import haxe.unit.TestCase;
using florentin.util.IteratorUtils;
using florentin.util.MathUtils;
using florentin.util.MapUtils;

class TestMathUtils extends TestCase {
    public function testTarjanStronglyConnectedComponents() {
        var vs = ["A", "B", "C", "D", 
                  "E", "F", "G", "H"];
        var es = [
            "A" => ["E"],
            "B" => ["A"],
            "C" => ["B", "D"],
            "D" => ["C"],
            "E" => ["B"],
            "F" => ["B", "E", "G"],
            "G" => ["C", "F"],
            "H" => ["D", "G", "H"]
        ];
        var cs = vs.tarjanStronglyConnectedComponents(es.partialFun());
        var csString =cs.showElements(c->
            "(" + c.showElements(v->v, ", ") + ")", ", "
        );
        assertEquals("(B, E, A), (D, C), (G, F), (H)", csString);

    }
}
