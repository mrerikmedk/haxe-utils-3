package;

import haxe.PosInfos;
import haxe.unit.TestCase;
using florentin.util.Process;

class TestProcess extends TestCase {
    public function testMbindAsSeq() {
        bindLike(ProcessUtils.mbind);
    }
    public function testAndAsSeq() {
        seqLike(function(p1, p2) return 
            ProcessUtils.andCombine(p1, p2, function(v1, v2) return v2)
        );
    }



    private function seqLike(
        seq : Process<Int>->Process<Int>->Process<Int>, 
        ?pos : PosInfos
    ) {
        {   // resolved, resolved
            var p = seq(7.resolved(), 8.resolved());
            switch (p) {
                case Resolved(v): assertEquals(8, v, pos);
                default: assertTrue(false, pos);
            }
        }
        {
            // resolved, failed
            var p = seq(7.resolved(), "fail".failed());
            switch (p) {
                case Failed(e): assertEquals("fail", e, pos);
                default: assertTrue(false, pos);
            }
        }
        {
            // resolved, async resolve
            var p = seq(7.resolved(), 8.delay(10));
            switch (p) {
                case Async(f): f(
                    function(v) assertEquals(8, v, pos),
                    function(e) assertTrue(false, pos)
                );
                default: assertTrue(false, pos);
            }
        }
        {
            // resolved, async fail
            var p = seq(7.resolved(), "fail".delayFail(10));
            switch (p) {
                case Async(f): f(
                    function(v) assertTrue(false, pos),
                    function(e) assertEquals("fail", e, pos)
                );
                default: assertTrue(false, pos);
            }
        }
        {
            // failed, resolved
            var p = seq("fail".failed(), 8.resolved());
            switch (p) {
                case Failed(e): assertEquals("fail", e, pos);
                default: assertTrue(false, pos);
            }
        }
        {
            // failed, failed
            var p = seq("fail".failed(), "fail".failed());
            switch (p) {
                case Failed(e): assertEquals("fail", e, pos);
                default: assertTrue(false, pos);
            }
        }
        {
            // failed, async resolve
            var p = seq("fail".failed(), 8.delay(10));
            switch (p) {
                case Failed(e): assertEquals("fail", e, pos);
                default: assertTrue(false, pos);
            }
        }
        {
            // failed, async fail
            var p = seq("fail".failed(), "fail".delayFail(10));
            switch (p) {
                case Failed(e): assertEquals("fail", e, pos);
                default: assertTrue(false, pos);
            }
        }
        {
            // async resolve, resolve
            var p = seq(7.delay(10), 8.resolved());
            switch (p) {
                case Async(f): f(
                    function(v) assertEquals(8, v, pos),
                    function(e) assertTrue(false, pos)
                );
                default: assertTrue(false, pos);
            }
        }
        {
            // async resolve, failed
            var p = seq(7.delay(10), "fail".failed());
            switch (p) {
                case Failed(e): assertEquals("fail", e, pos);
                default: assertTrue(false, pos);
            }
        }
        {
            // async resolve, async resolve
            var p = seq(7.delay(10), 8.delay(10));
            switch (p) {
                case Async(f): f(
                    function(v) assertEquals(8, v, pos),
                    function(e) assertTrue(false, pos)
                );
                default: assertTrue(false, pos);
            }
        }
        //TODO: async fail...

    }

    private function bindLike(
        bindLike : Process<Int>->(Int->Process<Int>)->Process<Int>, 
        ?pos : PosInfos
    ) {
        {   // resolved, resolved
            var p = bindLike(7.resolved(), function(v1) return (v1 + 3).resolved());
            switch (p) {
                case Resolved(v): assertEquals(10, v, pos);
                default: assertTrue(false, pos);
            }
        }
        {
            // resolved, failed
            var p = bindLike(7.resolved(), function(v1) return "fail".failed());
            switch (p) {
                case Failed(e): assertEquals("fail", e, pos);
                default: assertTrue(false, pos);
            }
        }
        {
            // resolved, async resolve
            var p = bindLike(7.resolved(), function(v1) return (v1 + 3).delay(10));
            switch (p) {
                case Async(f): f(
                    function(v) assertEquals(10, v, pos),
                    function(e) assertTrue(false, pos)
                );
                default: assertTrue(false, pos);
            }
        }
        {
            // resolved, async fail
            var p = bindLike(7.resolved(), function(v1) return "fail".delayFail(10));
            switch (p) {
                case Async(f): f(
                    function(v) assertTrue(false, pos),
                    function(e) assertEquals("fail", e, pos)
                );
                default: assertTrue(false, pos);
            }
        }
        {
            // failed, resolved
            var p = bindLike("fail".failed(), function(v1) return (v1 + 3).resolved());
            switch (p) {
                case Failed(e): assertEquals("fail", e, pos);
                default: assertTrue(false, pos);
            }
        }
        {
            // failed, failed
            var p = bindLike("fail".failed(), function(v1) return "fail".failed());
            switch (p) {
                case Failed(e): assertEquals("fail", e, pos);
                default: assertTrue(false, pos);
            }
        }
        {
            // failed, async resolve
            var p = bindLike("fail".failed(), function(v1) return (v1 + 3).delay(10));
            switch (p) {
                case Failed(e): assertEquals("fail", e, pos);
                default: assertTrue(false, pos);
            }
        }
        {
            // failed, async fail
            var p = bindLike("fail".failed(), function(v1) return "fail".delayFail(10));
            switch (p) {
                case Failed(e): assertEquals("fail", e, pos);
                default: assertTrue(false, pos);
            }
        }
        {
            // async resolve, resolve
            var p = bindLike(7.delay(10), function(v1) return (v1 + 3).resolved());
            switch (p) {
                case Async(f): f(
                    function(v) assertEquals(10, v, pos),
                    function(e) assertTrue(false, pos)
                );
                default: assertTrue(false, pos);
            }
        }
        {
            // async resolve, failed
            var p = bindLike(7.delay(10), function(v1) return "fail".failed());
            switch (p) {
                case Async(f): f(
                    function(v) assertTrue(false, pos),
                    function(e) assertEquals("fail", e, pos)
                );
                default: assertTrue(false, pos);
            }
        }
        {
            // async resolve, async resolve
            var p = bindLike(7.delay(10), function(v1) return (v1 + 3).delay(10));
            switch (p) {
                case Async(f): f(
                    function(v) assertEquals(10, v, pos),
                    function(e) assertTrue(false, pos)
                );
                default: assertTrue(false, pos);
            }
        }
        {
            // async resolve, async fail
            var p = bindLike(7.delay(10), function(v1) return "fail".delayFail(10));
            switch (p) {
                case Async(f): f(
                    function(v) assertTrue(false, pos),
                    function(e) assertEquals("fail", e, pos)
                );
                default: assertTrue(false, pos);
            }
        }
        //TODO: async fail...

    }
}