package;

using Lambda;
import haxe.PosInfos;
import haxe.unit.TestCase;
using florentin.util.ArrayUtils;
using florentin.util.IteratorUtils;
using florentin.util.MapUtils;
using florentin.util.Map2Utils;

class TestMap2Utils extends TestCase {
    public function testSet() {
        var map = new Map2<String, String, String>();
        map.set2("foo", "bar", "foobar");
        assertEqMap2(["foo"=>["bar"=>"foobar"]], map);

        map.set2("bar", "foo", "barfoo");
        assertEqMap2([
            "foo"=>["bar"=>"foobar"],
            "bar"=>["foo"=>"barfoo"]
        ], map);

        map.set2("foo", "baz", "foobaz");
        assertEqMap2([
            "foo"=>[
                "bar"=>"foobar",
                "baz"=>"foobaz"
            ],
            "bar"=>["foo"=>"barfoo"]
        ], map);

        map.set2("foo", "bar", "changed");
        assertEqMap2([
            "foo"=>[
                "bar"=>"changed",
                "baz"=>"foobaz"
            ],
            "bar"=>["foo"=>"barfoo"]
        ], map);
    }

    public function testHas() {
        var map = new Map2<String, String, String>();
        assertFalse(map.has2("foo", "bar"));

        map.set2("foo", "bar", "foobar");
        assertTrue(map.has2("foo", "bar"));
        assertFalse(map.has2("foo", "baz"));
        assertFalse(map.has2("bar", "foo"));
    }

    public function testGet() {
        var map = new Map2<String, String, String>();
        assertEquals(null, map.get2("foo", "bar"));

        map.set2("foo", "bar", "foobar");
        assertEquals("foobar", map.get2("foo", "bar"));
        assertEquals(null, map.get2("foo", "baz"));
        assertEquals(null, map.get2("bar", "foo"));
    }

    public function testRemove() {
        var map = new Map2<String, String, String>();
        map.remove2("foo", "bar");
        assertEqMap2(new Map2<String, String, String>(), map);

        map.set2("foo", "bar", "foobar");
        map.set2("foo", "baz", "foobaz");
        map.set2("bar", "foo", "barfoo");
        map.remove2("foo", "bar");
        assertEqMap2([
            "foo"=>["baz"=>"foobaz"],
            "bar"=>["foo"=>"barfoo"]
        ], map);
        map.remove2("foo", "baz");
        assertEqMap2([
            "bar"=>["foo"=>"barfoo"]
        ], map);
    }

    private function assertEqMap2<TK1, TK2, TV>(
        m1 : Map2<TK1, TK2, TV>,
        m2 : Map2<TK1, TK2, TV>,
        ?pos : PosInfos
    ) : Void {
        assertEqByMap2(m1, m2, function(a, b) return a == b, pos);
    }
    private function assertEqByMap2<TK1, TK2, TV>(
        m1 : Map2<TK1, TK2, TV>,
        m2 : Map2<TK1, TK2, TV>,
        eqValue : TV->TV->Bool,
        ?pos : PosInfos
    ) : Void {
        var k11 = m1.keys2().array();
        var k12 = m2.keys2().array();
        assertTrue(k11.isPermutationSlow(k12), pos);
        for (k in k11) {
            var m12 = m1.get(k);
            var m22 = m2.get(k);
            var ps1 = m12.pairs().array();
            var ps2 = m22.pairs().array();
            assertTrue(ps1.isPermutationBySlow(ps2, 
                function(p1, p2) return p1.key == p2.key && p1.value == p2.value
            ));
        }

    }
}