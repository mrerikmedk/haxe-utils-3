package;

using florentin.util.Failable;
import haxe.unit.TestCase;

class TestFailable extends TestCase {
    public function testMbindOO() {
        var o = Ok(3).mbind(function(v) return Ok(v + 7));
        switch (o) {
            case Ok(v): assertEquals(10, v);
            case Failed(e): assertTrue(false);
        }
    }

    public function testMbindFO() {
        var o = Failed("fail").mbind(function(v) return Ok(v + 7));
        switch (o) {
            case Ok(v): assertTrue(false);
            case Failed(e): assertEquals("fail", e);
        }
    }

    public function testMbindOF() {
        var o = Ok(3).mbind(function(v) return Failed("fail"));
        switch (o) {
            case Ok(v): assertTrue(false);
            case Failed(e): assertEquals("fail", e);
        }
    }


    public function testMbindFF() {
        var o = Failed("fail1").mbind(function(v) return Failed("fail2"));
        switch (o) {
            case Ok(v): assertTrue(false);
            case Failed(e): assertEquals("fail1", e);
        }
    }
}